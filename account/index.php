<?php
ini_set("display_errors", "1");
include_once $_SERVER["DOCUMENT_ROOT"] . '/khartn_cms/system/init/header.php';
?>

<div class="content-wrapper">
    <?php
    if ($_SESSION['uname'] == "") {
        APPLICATION::redirectToPage("/");
    } else {
        APPLICATION::includeComponent("khartn:account");
    }
    ?>

</div>

<?php
include_once $_SERVER["DOCUMENT_ROOT"] . '/khartn_cms/system/init/footer.php';
?>
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.mavenproject9;

import java.io.IOException;
import java.io.PrintWriter;
import javax.jws.soap.SOAPBinding;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

/**
 *
 * @author wwwdev
 */
public class NewServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            Document doc = Jsoup.connect(request.getParameter("url")).get();

//            System.out.println("***");
//            System.out.println(doc.select("*[src*=jquery]"));

            doc.select("*[src*=jquery.min]").remove();

            doc.select("head").append("<link rel=\"stylesheet\" href=\"http://landings.pw/khartn_cms/templates/editor/js/jquery-ui/css/smoothness/jquery-ui-1.10.3.custom.css\" />");
            doc.select("head").append("<script type=\"text/javascript\" src=\"http://code.jquery.com/jquery-2.0.3.js\"></script>");
            doc.select("head").append("<script type=\"text/javascript\" src=\"http://landings.pw/khartn_cms/templates/editor/js/jquery-ui/js/jquery-ui-1.10.3.custom.js\"></script>");
            doc.select("head").append("<script type=\"text/javascript\" src=\"http://landings.pw/khartn_cms/templates/land/js/main/main_for_call.js\"></script>");
            doc.select("head").append("<script type=\"text/javascript\" src=\"http://landings.pw/khartn_cms/templates/editor/js/jquery.filedrop.js\"></script>");
            doc.select("head").append("<link rel=\"stylesheet\" href=\"http://landings.pw/khartn_cms/templates/land/fontello/css/fontello.css\">");
            doc.select("head").append("<link rel=\"stylesheet\" href=\"http://landings.pw/khartn_cms/templates/editor/css/styles.css\">");

            out.println(doc.html());

        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

package name.khartn.simple_script_insert;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

/**
 *
 * @author wwwdev
 */
public class insert extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
//            out.println("Processing url "+request.getParameter("url"));

            Document doc = Jsoup.connect(request.getParameter("url")).get();

            doc.select("*[src*=jquery.min]").remove();
            doc.select("*[href*=jquery-ui]").remove();            
            doc.select("*[href*=fontello]").remove();            
            doc.select("*[href*=fonts]").remove();            
//            doc.select("*[href*=/uploads/landings-css/]").remove();            
            doc.select("*[href*=http://landings.pw/khartn_cms/templates/editor/css/styles.css]").remove();            
            doc.select("*[src*=jquery-ui]").remove();
            doc.select("*[src*=jquery]").remove();
            doc.select("*[src*=piwik]").remove();
            doc.select("*[src*=landingi]").remove();
            doc.select("*[src*=relic]").remove();
            doc.select("*[src*=main_for_call.js]").remove();
            doc.select("*[src*=jquery.filedrop.js]").remove();
            
            doc.select("body").select("script").remove();
            
            doc.select("head").append("<script type=\"text/javascript\" src=\"http://code.jquery.com/jquery-2.0.3.js\"></script>");
            doc.select("head").append("<link rel=\"stylesheet\" href=\"http://landings.pw/khartn_cms/templates/editor/js/jquery-ui/css/smoothness/jquery-ui-1.10.3.custom.css\" />");
//            doc.select("head").append("<link rel=\"stylesheet\" href=\"http://landings.pw/khartn_cms/templates/editor/js/jquery-editable-select/style.css\" />");
//            doc.select("head").append("<script type=\"text/javascript\" src=\"http://landings.pw/khartn_cms/templates/editor/js/jquery-editable-select/jquery.editableSelect.js\"></script>");
            doc.select("head").append("<script type=\"text/javascript\" src=\"http://landings.pw/khartn_cms/templates/editor/js/jquery-ui/js/jquery-ui-1.10.3.custom.js\"></script>");
            doc.select("head").append("<script type=\"text/javascript\" src=\"http://landings.pw/khartn_cms/templates/editor/js/main/main_for_call.js\"></script>");
            doc.select("head").append("<script type=\"text/javascript\" src=\"http://landings.pw/khartn_cms/templates/editor/js/jquery.filedrop.js\"></script>");
            doc.select("head").append("<link rel=\"stylesheet\" href=\"http://landings.pw/khartn_cms/templates/land/fontello/css/fontello.css\" />");
            doc.select("head").append("<link rel=\"stylesheet\" href=\"http://landings.pw/khartn_cms/templates/editor/css/styles_for_call.css\" />");

            out.println(doc.html());
        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

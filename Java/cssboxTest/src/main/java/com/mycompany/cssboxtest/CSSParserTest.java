package com.mycompany.cssboxtest;

import com.osbcp.cssparser.CSSParser;
import com.osbcp.cssparser.Rule;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class CSSParserTest {

    public static void main(String[] args) {
        try {
            List<Rule> rules = CSSParser.parse("body,\n"
                    + "th,\n"
                    + "td,\n"
                    + "input,\n"
                    + "textarea,\n"
                    + "select,\n"
                    + "option { font-family:Arial,Helvetica,sans-serif; }\n"
                    + "\n"
                    + "h1,\n"
                    + "h2,\n"
                    + "h3 {\n"
                    + "  text-transform:lowercase;\n"
                    + "  font-family:\"Trebuchet MS\",Arial,Helvetica,sans-serif;\n"
                    + "  font-weight:normal;\n"
                    + "  color:#FFFFFF;\n"
                    + "}"
                    + "a { color:#FFEA6F; }\n"
                    + "\n"
                    + "a:hover { text-decoration:none; "
                    + "}"
                    + ""
                    + "#logo {\n"
                    + "  height:170px;\n"
                    + "  background:url(/landings/templates/ba91309b-dd98-4316-9908-37c043489fd7/images/efb38973-e225-480d-aa6e-dfd686bc6231.gif) no-repeat left 65%;\n"
                    + "}\n"
                    + "\n"
                    + "#logo h1 {\n"
                    + "  float:left;\n"
                    + "  padding:40px 40px 0 50px;\n"
                    + "  letter-spacing:-2px;\n"
                    + "  font-size:48px;\n"
                    + "}\n"
                    + "\n"
                    + "#logo h2 {\n"
                    + "  float:right;\n"
                    + "  padding:68px 0 0 0;\n"
                    + "  font-size:24px;\n"
                    + "}\n"
                    + "\n"
                    + "#logo a {\n"
                    + "  text-decoration:none;\n"
                    + "  color:#372412;\n"
                    + "}");
            for (Rule rule : rules) {

                System.out.println(rule.getSelectors());
                System.out.println(rule.toString());
                System.out.println("++++++++++++++++");

            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package name.khartn.templateprocessorstatic;

import com.osbcp.cssparser.CSSParser;
import com.osbcp.cssparser.Rule;
import com.osbcp.cssparser.Selector;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.io.IOUtils;

/**
 *
 * @author wwwdev
 */
public class ProcessOldStyles extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            /* TODO output your page here. You may use following sample code. */
//            String[] allStyles = request.getParameterValues("allStyles");
            String lastStyleFile = request.getParameter("lastStyleFile");

            Map<String, String[]> reqParamMap = request.getParameterMap();
            Set<String> keyset = reqParamMap.keySet();
            ArrayList<String> stylesAll = new ArrayList<String>();
            for (String key : keyset) {
                if (key.contains("allStyles")) {
                    for (String val : reqParamMap.get(key)) {
//                        out.println(val);
                        stylesAll.add("." + val);
                    }
                }
            }

//            out.println(stylesAll);
//            out.println("***");
            if (lastStyleFile != null) {

                String contents = IOUtils.toString(new URL("http://landings.pw" + lastStyleFile).openStream());
                List<Rule> rules = CSSParser.parse(contents);
                String stringWithNeededStyles = "";
                for (Rule rule : rules) {

                    for (Selector selector : rule.getSelectors()) {
                        String selName = selector.toString();
//                    out.println(selName);
                        if (stylesAll.contains(selName)) {
                            stringWithNeededStyles += rule.toString() + "   ";
                        }
                    }

                }
                out.println(stringWithNeededStyles);
            } else {
                out.println("is null");
            }

//           out.println();
        } catch (Exception ex) {
            out.println("error");
            ex.printStackTrace(out);
        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

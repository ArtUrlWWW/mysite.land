package name.khartn.templateprocessorstatic;

import java.io.File;
import java.io.FileOutputStream;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.util.UUID;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

/**
 *
 * @author wwwdev
 */
public class CSSProcessor {

    public void process(Element cssElement, String baseUrl, String dirName, String uid) {
        String href = "";
        try {
            href = cssElement.attr("href");

            String baseUrlTmp = baseUrl.substring(baseUrl.indexOf("://") + 3);
            String domain = baseUrlTmp.substring(0, baseUrlTmp.indexOf("/"));

            if (href.startsWith("/")) {
                href = baseUrl.substring(0, baseUrl.indexOf("://") - 1) + domain + href;
            } else if (!href.startsWith("/") && !href.contains("://")) {
//                System.out.println("+++"+href);
                String baseUrl1;
                if (baseUrl.endsWith(".html")) {
                    baseUrl1 = baseUrl.substring(0, baseUrl.lastIndexOf("/") + 1);
                } else {
                    baseUrl1 = baseUrl;
                }

                href = baseUrl1 + href;

            }

            System.out.println("trying to process link " + href);

            String domainFull = baseUrl.substring(0, baseUrl.indexOf("://") + 3) + domain;

            UUID uuid = UUID.randomUUID();
            String randomUUIDString = uuid.toString();

            File cssDir = new File(dirName + "/css/");
            cssDir.mkdirs();

            String fileName = dirName + "css/" + randomUUIDString + ".css";

            try {
                URL website = new URL(href);
                ReadableByteChannel rbc = Channels.newChannel(website.openStream());

                System.out.println("file " + fileName);
                System.out.println("href " + href);

                File outCSSfile = new File(fileName);
                FileOutputStream fos = new FileOutputStream(outCSSfile);
                fos.getChannel().transferFrom(rbc, 0, Long.MAX_VALUE);

                WikiVisitUrls wikiVisitUrls = new WikiVisitUrls();
                wikiVisitUrls.readFromStyleAttributeWithAPI(outCSSfile, baseUrl, dirName, href);

                cssElement.attr("href", "/" + fileName);
            } catch (Exception e) {
                System.out.println(href);
                e.printStackTrace();
            }

        } catch (Exception e) {
            System.out.println("Error in CSSProcessor");
            e.printStackTrace();
        }

    }
}

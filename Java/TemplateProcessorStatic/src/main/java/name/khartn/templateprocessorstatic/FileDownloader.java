package name.khartn.templateprocessorstatic;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.util.UUID;

/**
 *
 * @author wwwdev
 */
public class FileDownloader {

    public String download(String pathToFile, String baseUrl, String dirName) {
        try {

            System.out.println("Step 1");
            System.out.println("pathToFile " + pathToFile);
            System.out.println("baseUrl " + baseUrl);
            System.out.println("dirName " + dirName);

            if (pathToFile.indexOf("?") > -1) {
                pathToFile = pathToFile.substring(0, pathToFile.lastIndexOf("?") );
            }

            String baseUrlTmp = baseUrl.substring(baseUrl.indexOf("://") + 3);
            String domain = baseUrlTmp.substring(0, baseUrlTmp.indexOf("/"));
            String domainFull = baseUrl.substring(0, baseUrl.indexOf("://") + 3) + domain;

            if (pathToFile.startsWith("/") && !pathToFile.startsWith("//")) {
                pathToFile = baseUrl.substring(0, baseUrl.indexOf("://") + 3) + domain + pathToFile;
//                System.out.println("1!!!");
            } else if (pathToFile.startsWith("/") && pathToFile.startsWith("//")) {
                pathToFile = baseUrl.substring(0, baseUrl.indexOf("://") + 1) + pathToFile;
//                System.out.println(pathToFile);
            } else if (!pathToFile.startsWith("/") && !pathToFile.contains("://")) {
                pathToFile = baseUrl.substring(0, baseUrl.lastIndexOf("/") + 1) + pathToFile;
//                System.out.println("2!!!");
            }

            String ext = pathToFile.substring(pathToFile.lastIndexOf(".") + 1);
            String fileNameOrig = pathToFile.substring(pathToFile.lastIndexOf("/") + 1);

//            UUID uuid = UUID.randomUUID();
//            String randomUUIDString = uuid.toString();
            File imagesDir = new File(dirName + "/images/");
            imagesDir.mkdirs();

//            System.out.println(dirName);
//            String fileName = dirName + "images/" + randomUUIDString + "." + ext;
            String fileName = dirName + "images/" + fileNameOrig;

            System.out.println("pathToFile " + pathToFile + " " + fileName);

            System.out.println("Step 2");
            System.out.println("pathToFile " + pathToFile);

            URL website = new URL(pathToFile);

            try {
                ReadableByteChannel rbc = Channels.newChannel(website.openStream());
                File outCSSfile = new File(fileName);
                FileOutputStream fos = new FileOutputStream(outCSSfile);
                fos.getChannel().transferFrom(rbc, 0, Long.MAX_VALUE);
            } catch (Exception e) {
                System.out.println(pathToFile);
                e.printStackTrace();
                return "!!!ERROR!!!";
            }

            return "/" + fileName;
        } catch (IOException ex) {
            System.out.println("Error in FileDownloader");
            ex.printStackTrace();
            return "!!!ERROR!!!";
        }
    }

}

package name.khartn.templateprocessorstatic;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.OutputStreamWriter;
import java.util.UUID;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 * Hello world!
 *
 */
public class App {

    public static void main(String[] args) {
        try {

            UUID uuid = UUID.randomUUID();
            String uid = uuid.toString();
            String dirName = "landings/templates/" + uid + "/";

            File downloadDir = new File(dirName);
            downloadDir.mkdirs();

//            String baseUrl = "http://images.all-free-download.com/free-website-templates-preview/natures_charm_261/";
//            String baseUrl = "http://images.all-free-download.com/free-website-templates-preview/interior_design_template_2058/";
//            String baseUrl = "http://images.all-free-download.com/free-website-templates-preview/natures_charm_261/";
//            String baseUrl = "http://prwnowr2.dotests.com/";
//            String baseUrl = "http://3vhk9yrd.dotests.com/";
//            String baseUrl = "http://vivaco.com/demo/stylio_v2/index-img.html";
            String baseUrl = "http://eduland.coralixthemes.com/blue/";

            File outHTMLfile = new File(dirName + uid + ".html");

            Document doc = Jsoup.connect(baseUrl).get();

            Elements links = doc.select("link[rel=stylesheet]");
            CSSProcessor cssProcessor = new CSSProcessor();
            for (Element link : links) {
                cssProcessor.process(link, baseUrl, dirName, uid);
            }

            Elements imgs = doc.getElementsByTag("img");
            FileDownloader fd = new FileDownloader();
            for (Element img : imgs) {
//                System.out.println(img.attr("src"));
                img.attr("src", fd.download(img.attr("src"), baseUrl, dirName));
            }

            Elements scripts = doc.getElementsByTag("script");
            for (Element script : scripts) {
                if (script.attr("src") != "") {
//                    System.out.println(script.attr("src"));
                    script.attr("src", fd.download(script.attr("src"), baseUrl, dirName));
                }
            }

            BufferedWriter out = new BufferedWriter(
                    new OutputStreamWriter(
                    new FileOutputStream(outHTMLfile),"UTF-8")
            );
            out.write(doc.html());
            out.close();

        } catch (Exception ex) {
            System.out.println("Error in App");
            ex.printStackTrace();;

        }
    }
}

package name.khartn.templateprocessorstatic;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import com.phloc.commons.charset.CCharset;
import com.phloc.commons.io.file.SimpleFileIO;
import com.phloc.css.CSSSourceLocation;
import com.phloc.css.ECSSVersion;
import com.phloc.css.decl.CSSDeclaration;
import com.phloc.css.decl.CSSExpressionMemberTermURI;
import com.phloc.css.decl.CSSImportRule;
import com.phloc.css.decl.CascadingStyleSheet;
import com.phloc.css.decl.ICSSTopLevelRule;
import com.phloc.css.decl.visit.CSSVisitor;
import com.phloc.css.decl.visit.DefaultCSSUrlVisitor;
import com.phloc.css.reader.CSSReader;
import com.phloc.css.writer.CSSWriter;
import com.phloc.css.writer.CSSWriterSettings;
import java.io.File;

/**
 * Example how to extract all URLs from a certain CSS file using an
 * ICSSUrlVisitor.
 *
 * @author Philip Helger
 */
public final class WikiVisitUrls {

    public static String getSourceLocationString(@Nonnull final CSSSourceLocation aSourceLoc) {
        return "source location reaches from ["
                + aSourceLoc.getFirstTokenBeginLineNumber()
                + "/"
                + aSourceLoc.getFirstTokenBeginColumnNumber()
                + "] up to ["
                + aSourceLoc.getLastTokenEndLineNumber()
                + "/"
                + aSourceLoc.getLastTokenEndColumnNumber()
                + "]";
    }

    public void readFromStyleAttributeWithAPI(File outCSSfile, final String baseUrl, final String dirName, final String cssFileURL) {
        try {

//            final String sStyle = "@import 'foobar.css';\n"
//                    + "div{background:fixed url(a.gif) !important;}\n"
//                    + "span { background-image:url('/my/folder/b.gif');}";
            final CascadingStyleSheet aCSS = CSSReader.readFromFile(outCSSfile, CCharset.CHARSET_UTF_8_OBJ, ECSSVersion.CSS30);
//    final CascadingStyleSheet aCSS = CSSReader.readFromString (sStyle, CCharset.CHARSET_UTF_8_OBJ, ECSSVersion.CSS30);
            CSSVisitor.visitCSSUrl(aCSS, new DefaultCSSUrlVisitor() {
                // Called for each import
                @Override
                public void onImport(@Nonnull final CSSImportRule aImportRule) {
                    System.out.println("Import: "
                            + aImportRule.getLocationString()
                            + " - "
                            + getSourceLocationString(aImportRule.getSourceLocation()));
                }

                // Call for URLs outside of URLs
                @Override
                public void onUrlDeclaration(@Nullable final ICSSTopLevelRule aTopLevelRule,
                        @Nonnull final CSSDeclaration aDeclaration,
                        @Nonnull final CSSExpressionMemberTermURI aURITerm) {
//                aURITerm.setURIString("/qqqq");
//                System.out.println(aURITerm.getURIString());
                    FileDownloader fd = new FileDownloader();
                    
//                    String cssFileFolder=cssFileURL.substring(0, cssFileURL.lastIndexOf("/"));

                    aURITerm.setURIString(fd.download(aURITerm.getURIString(), cssFileURL, dirName));

//                System.out.println(aDeclaration.getProperty()
//                        + " - references: "
//                        + aURITerm.getURIString()
//                        + " - "
//                        + getSourceLocationString(aURITerm.getSourceLocation()));
                }
            });

            final CSSWriterSettings aSettings = new CSSWriterSettings(ECSSVersion.CSS30, false);
            final CSSWriter aWriter = new CSSWriter(aSettings);
            // Write the @charset rule: (optional)
            aWriter.setContentCharset("utf-8");
            // Write a nice file header
            aWriter.setHeaderText("This file was generated by phloc-css\nGrab a copy at http://code.google.com/p/phloc-css");
            // Convert the CSS to a String
            final String sCSSCode = aWriter.getCSSAsString(aCSS);
//
//        System.out.println(sCSSCode);

            SimpleFileIO.writeFile(outCSSfile, sCSSCode, CCharset.CHARSET_UTF_8_OBJ);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

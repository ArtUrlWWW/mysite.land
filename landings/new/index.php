<?php
//ini_set("display_errors", "1");
include_once $_SERVER["DOCUMENT_ROOT"] . '/khartn_cms/system/init/header.php';
if ($_SESSION['uname'] == "") {
    APPLICATION::redirectToPage("/" . APPLICATION::$requestsProcessor->lang . "/login");
}
?>
<div class="content-wrapper">
    <?php
    APPLICATION::includeComponent("khartn:landings.create.new");
    ?>

</div>
<?php
include_once $_SERVER["DOCUMENT_ROOT"] . '/khartn_cms/system/init/footer.php';
?>
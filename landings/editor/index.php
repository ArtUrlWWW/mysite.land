<?php

//ini_set("display_errors", "1");
include_once $_SERVER["DOCUMENT_ROOT"] . '/khartn_cms/system/init/header.php';
if ($_SESSION['uname'] == "") {
    APPLICATION::redirectToPage("/" . APPLICATION::$requestsProcessor->lang . "/login");
}

//APPLICATION::setShowCleanOutput(TRUE);
Config::$defaultTemplate = "editor";
?>

<?php

APPLICATION::includeComponent("khartn:landings.create.new.editor");
?>


<?php

include_once $_SERVER["DOCUMENT_ROOT"] . '/khartn_cms/system/init/footer.php';
?>
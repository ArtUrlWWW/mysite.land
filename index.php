<?php
ini_set("display_errors", "1");
include_once $_SERVER["DOCUMENT_ROOT"] . '/khartn_cms/system/init/header.php';
?>

<div class="content-wrapper">
    <?php
    if ($_SESSION['uname'] == "") {
        APPLICATION::includeComponent("khartn:index.page");
    } else {
        APPLICATION::includeComponent("khartn:landings.main.page");
    }
    ?>

</div>

<?php
include_once $_SERVER["DOCUMENT_ROOT"] . '/khartn_cms/system/init/footer.php';
?>
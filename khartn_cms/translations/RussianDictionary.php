<?php

class Dict {

    private function __construct() {
        
    }

    /**
     * Прозрачность
     * @var type 
     */
    static $bgColorOpacity = "Прозрачность";

    /**
     * Цвет текста
     * @var type 
     */
    static $textColor = "Цвет текста";

    /**
     * Цвет
     * @var type 
     */
    static $color = "Цвет";

    /**
     * Сохранить
     * @var type 
     */
    static $save = "Сохранить";

    /**
     * Первая фраза на главной странице.
     * @var string
     */
    static $mainPagePhrase1 = "Free hosting for landing pages. <br/ > Бесплатный landing (лэндинг) хостинг. <br /> На данный момент хостинг находится на Beta разработки и на данный момент абсолютно бесплатен. <br />  Функционал системы постоянно дополняется новыми функциями. <br /> <br /> <a href='/ru/reg'>Регистрация</a> <br /> <br /> <a href='/ru/login'>Войти в систему</a>";

    /**
     * Редактировать
     * @var type 
     */
    static $edit = "Редактировать";

    /**
     * Номер лэндинга
     * @var string  $landingsID
     */
    static $landingsID = "Номер лэндинга";

    /**
     * Background
     * 
     */
    static $background = "Фон";

    /**
     * Создать с чистой страницы
     */
    static $createNewLandingPageFromScratch = "Создать с чистой страницы";

    /**
     * Регистрация
     * @var string
     */
    static $register = "Регистрация";

    /**
     * Создать новую landing страницу
     */
    static $createNewLandingPage = "Создать новую landing страницу";

    /**
     * Лэндинги
     * @var type 
     */
    static $landings = "Лэндинги";

    /**
     * Аккаунт
     * @var string
     */
    static $account = "Аккаунт";

    /**
     * Некорректный логин или пароль
     * @var string
     */
    static $authWrongLoginOrPassword = "Некорректный логин или пароль";

    /**
     * Данный E-mail уже был зарегистрирован
     * @var string 
     */
    static $thisEmailIsAlreadyRegistered = "Данный E-mail уже был зарегистрирован";

    /**
     * Не корректно введён номер телефона
     * @var string 
     */
    static $incorrectPhone = "Не корректно введён номер телефона";

    /**
     * Длина пароля слишком мала.
     * @var string 
     */
    static $incorrectPasswd = "Длина пароля слишком мала.";

    /**
     * Не правильно введён E-mail
     * @var string
     */
    static $wrongEmailFormat = "Не правильно введён E-mail";

    /**
     * Phone: 
     */
    static $phoneVariant1 = "Телефон (в виде 7 123 777 77 77): ";

    /**
     * Password: 
     */
    static $passwdVariant1 = "Пароль (не менее 7 символов): ";

    /**
     * Email: 
     */
    static $emailVariant1 = "Email: ";

    /**
     * Не правильно введён код с рисунка
     * @var string
     */
    static $wrongCaptcha = "Не правильно введён код с рисунка";

    /**
     * 15 пробных дней
     * @var string
     */
    static $probaPytnadcatDnye = "15 пробных дней";

    /**
     * Главная
     *  @var string 
     * 
     */
    static $HOME = "Главная";

    /**
     *
     * Цены
     *  @var string 
     */
    static $PRICING = "Цены";

    /**
     * Блог
     * @var string  
     */
    static $BLOG = "Блог";

    /**
     * Контакты
     * @var string
     */
    static $CONTACTS = "Контакты";

    /**
     * Войти
     * @var string 
     */
    static $LOG_IN = "Войти";

}

?>

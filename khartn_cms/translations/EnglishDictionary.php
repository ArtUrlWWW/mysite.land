<?php

class Dict {

    private function __construct() {
        
    }
    
    /**
     * Прозрачность
     * @var type 
     */
    
    static $bgColorOpacity="Opacity";
    
    /**
     * Цвет текста
     * @var type 
     */
    
    static $textColor="Text color";
    
     /**
     * Цвет
     * @var type 
     */
    static $color="Color";
    
    /**
     * Сохранить
     * @var type 
     */
    static $save="Save";
    
    /**
     * Первая фраза на главной странице.
     * @var string
     */
    static $mainPagePhrase1= "Free hosting for landing pages. <br/ > Currently hosting is on the development and Beta at the moment is absolutely free. <br />  Functionality of the system is constantly updated with new features. <br /> <br /> <a href='/en/reg'>Registration</a> <br /> <br /> <a href='/en/login'>Log in</a>";
    
    
    /**
     * Редактировать
     * @var type 
     */    
    static $edit="Edit";
    
    /**
     * Номер лэндинга
     * @var string  $landingsID
     */
    static $landingsID = "Landing ID number";
    
    /**
     * Background
     * 
     */
    static $background="Background";


    /**
     * Создать с чистой страницы
     */    
    static $createNewLandingPageFromScratch="Create page from scratch";
    
    /**
     * Регистрация
     * @var string
     */
    static $register="Register";
    
    /**
     * Создать новую landing страницу
     */
    
    static $createNewLandingPage="Create new landing";
    
    /**
     * Landings
     * @var type 
     */
    static $landings="Landings";
    
    /**
     * Аккаунт
     * @var string
     */
    static $account="Account";
    
    /**
     * Некорректный логин или пароль
     * @var string
     */
    static $authWrongLoginOrPassword="Incorrect login";
    
    /**
     * Данный E-mail уже был зарегистрирован
     * @var string 
     */
    static $thisEmailIsAlreadyRegistered="This E-Mail is already registered";
    
    /**
     * Не корректно введён номер телефона
     * @var string 
     */
    static $incorrectPhone="Incorrect phone format";
    
     /**
     * Длина пароля слишком мала.
     * @var string 
     */
    static $incorrectPasswd="Password length is too small.";
    
    /**
     * Не правильно введён E-mail
     * @var string
     */
    static $wrongEmailFormat="Incorrect E-mail format";
    
    /**
     * Phone: 
     */
    static $phoneVariant1 = "Mob. Phone (format like): 1 123 123 123 123";
    
    /**
     * Password: 
     */
    static $passwdVariant1 = "Password (length>6 symbols): ";
    
    /**
     * Email: 
     */
    static $emailVariant1 = "Email: ";
    
    /**
     * Не правильно введён код с рисунка
     * @var string
     */
    static $wrongCaptcha="Wrong Captcha, please try again";

    /**
     * 15 пробных дней 
     * @var string
     */
    static $probaPytnadcatDnye="15-days free trial";


    /**
     * Главная
     *  @var string 
     * 
     */
    static $HOME = "Home";

    /**
     *
     * Цены
     *  @var string 
     */
    static $PRICING = "Pricing";

    /**
     * Блог
     * @var string  
     */
    static $BLOG = "Blog";

    /**
     * Контакты
     * @var string
     */
    static $CONTACTS = "Contact";

    /**
     * Войти
     * @var string 
     */
    static $LOG_IN = "Log in";

}

?>

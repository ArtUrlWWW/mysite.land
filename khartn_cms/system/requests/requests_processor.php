<?php

class Core_requests {

    protected static $_instance;
    private static $connect;

    private function __construct() {
        $this->reparseRrequest();
    }

    public function reparseRrequest() {
        // Убираем не нужные данные
        $this->valueFromURL = str_replace("?" . $_SERVER["QUERY_STRING"], '', $_SERVER['REQUEST_URI']);
        // Убираем слэши справа и слева строки
        $this->valueFromURL = preg_replace('/(^\/|\/$)/', '', $this->valueFromURL);
        // Преобразуем в массив
        $this->urlarray = explode('/', $this->valueFromURL);
    }

    private function __clone() {
        
    }

    public static function getInstance() {
        // проверяем актуальность экземпляра
        if (null === self::$_instance) {
            // создаем новый экземпляр
            self::$_instance = new self();
        }
        // возвращаем созданный или существующий экземпляр
        return self::$_instance;
    }

    /**
     * Преобразуем массив, созданный в конструкторе, в переменные, по шаблону,
     * переданному пользователем.
     * @param type $userPattern 
     */
    function get_vars($userPattern) {

        if (substr($userPattern, 0, 1) == "/") {
            $userPattern = substr($userPattern, 1, strlen($userPattern) - 1);
        }

        $userPatternArray = explode('/', $userPattern);

//        print_r($userPatternArray);

        foreach ($userPatternArray as $keyY => $varName) {
            if ($this->urlarray[$keyY] != '') {
                //создаём внутри данного класса необходимые переменные
//                echo $varName . " = " . $this->urlarray[$keyY] . "\r\n";
                $this->$varName = $this->urlarray[$keyY];
            } else {
                $this->$varName = '/';
//                echo $varName . " = / \r\n";
            }
        }
    }

    /**
     * Преобразуем массив, созданный в конструкторе, в переменные, по шаблону,
     * переданному пользователем, при этом, если кол-во элементов в шаблоне 
     * меньше, чем в ссылке, то оставшиеся элементы ссылки прибавляются к записи 
     * массива, с именем, равным последнему элемнту шаблона
     * @param type $userPattern 
     */
    function get_vars_extended($userPattern) {
        $userPatternArray = explode('/', $userPattern);
        if (count($userPatternArray) < count($this->urlarray)) {
            $lastName = "";
            foreach ($this->urlarray as $keyY => $varName) {
                if ($keyY < count($userPatternArray)) {
                    $lastName = $userPatternArray[$keyY];
                    $this->$lastName = $this->urlarray[$keyY];
                } else {
                    $this->$lastName .="/" . $this->urlarray[$keyY];
                }
            }
        } else {
            foreach ($userPatternArray as $keyY => $varName) {
                if (@$this->urlarray[$keyY] != '') {
                    //создаём внутри данного класса необходимые переменные
                    $this->$varName = $this->urlarray[$keyY];
                } else {
                    $this->$varName = '';
                }
            }
        }
    }

    function get_path_for_javadoc() {
        // Убираем не нужные данные
        $valueFromURL = str_replace("?" . $_SERVER["QUERY_STRING"], '', $_SERVER['REQUEST_URI']);
        // Убираем слэши справа и слева строки
        $valueFromURL = preg_replace('/(^\/|\/$)/', '', $valueFromURL);
        // Убираем 
        $valueFromURL = preg_replace('(^[A-Za-z]+/javadoc/[A-Za-z]+/)', '', $valueFromURL);
        return $valueFromURL;
    }

    function get_path_for_post_view() {
        return "http://" . $GLOBALS['Config']['MAIN_DOMAIN'] . str_replace(array(
                    "?" . $_SERVER["QUERY_STRING"], 'ajax/', 'getPost'), array('', '', 'view'), $_SERVER['REQUEST_URI']);
    }

}

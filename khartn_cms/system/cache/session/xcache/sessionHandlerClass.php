<?php

final class xCacheSessionHandler {

    public static function instance() {
        static $self = false;
        if (false === $self) {
            $self = new xCacheSessionHandler();
        }

        return $self;
    }

    private function __construct() {
        session_set_save_handler(
                array(__CLASS__, 'open'), array(__CLASS__, 'close'), array(__CLASS__, 'read'), array(__CLASS__, 'write'), array(__CLASS__, 'destroy'), array(__CLASS__, 'gc')
        );
    }

    public function __destruct() {
        session_write_close();
    }

    public static function open($save_path, $session_name) {
        return true;
    }

    public static function close() {
        return true;
    }

    public static function read($session_id) {
        return (string) xcache_get("session/{$session_id}");
    }

    public static function write($session_id, $session_data) {
        return xcache_set("session/{$session_id}", $session_data, (int) get_cfg_var('session.gc_maxlifetime'));
    }

    public static function destroy($session_id) {
        xcache_unset("session/{$session_id}");
        return true;
    }

    public static function gc($max_lifetime) {
        return true;
    }

}

?>

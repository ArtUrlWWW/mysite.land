<?php

final class MemcachedSessionHandler {

    /**
     *
     * @var Memcache 
     */
    private static $connect;

    public static function instance() {
        static $self = false;
        if (false === $self) {
            $self = new MemcachedSessionHandler();
        }

        return $self;
    }

    private function __construct() {

        self::$connect = new Memcache;
        self::$connect->pconnect('localhost', 11211) or die("Could not connect (session cache)");

        session_set_save_handler(
                array(__CLASS__, 'open'), array(__CLASS__, 'close'), array(__CLASS__, 'read'), array(__CLASS__, 'write'), array(__CLASS__, 'destroy'), array(__CLASS__, 'gc')
        );
    }

    public function __destruct() {
        session_write_close();
    }

    public static function open($save_path, $session_name) {
        return true;
    }

    public static function close() {
        return true;
    }

    public static function read($session_id) {
        return (string) self::$connect->get("session/{$session_id}");
    }

    public static function write($session_id, $session_data) {
        self::$connect->set("session/{$session_id}", $session_data, (int) get_cfg_var('session.gc_maxlifetime'));
    }

    public static function destroy($session_id) {
        self::$connect->delete("session/{$session_id}");
        return true;
    }

    public static function gc($max_lifetime) {
        return true;
    }

}

?>

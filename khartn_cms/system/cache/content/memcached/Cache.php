<?php

/**
 * Простой пример реализации паттерна Singleton
 *
 * @name SingletonTest
 * @author Andrew Vasiliev (illusive [at] meta [dot] ua)
 */
class Cache {

    /**
     * Статическая переменная, в которой мы
     * будем хранить экземпляр класса
     *
     * @var SingletonTest
     */
    protected static $_instance;
    private static $connect;

    /**
     * Закрываем доступ к функции вне класса.
     * Паттерн Singleton не допускает вызов
     * этой функции вне класса
     *
     */
    private function __construct() {
        /**
         * При этом в функцию можно вписать
         * свой код инициализации. Также можно
         * использовать деструктор класса.
         * Эти функции работают по прежднему,
         * только не доступны вне класса
         */
        self::$connect = new Memcache;
        self::$connect->pconnect('localhost', 11211);// or die("Could not connect (main cache)");
    }

    /**
     * Закрываем доступ к функции вне класса.
     * Паттерн Singleton не допускает вызов
     * этой функции вне класса
     *
     */
    private function __clone() {
        
    }

    /**
     * Статическая функция, которая возвращает
     * экземпляр класса или создает новый при
     * необходимости
     *
     * @return SingletonTest
     */
    public function getInstance() {
        // проверяем актуальность экземпляра
        if (null === self::$_instance) {
            // создаем новый экземпляр
            self::$_instance = new self();
        }
        // возвращаем созданный или существующий экземпляр
        return self::$_instance;
    }

}

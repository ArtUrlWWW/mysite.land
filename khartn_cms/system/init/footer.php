<?php

$middleContent = ob_get_clean();

// including head
if (!APPLICATION::$showCleanOutput) {
    include_once $_SERVER["DOCUMENT_ROOT"] . "/khartn_cms/templates/" . Config::$defaultTemplate . "/header.php";
}

echo $middleContent;

// including head
if (!APPLICATION::$showCleanOutput) {
    include_once $_SERVER["DOCUMENT_ROOT"] . "/khartn_cms/templates/" . Config::$defaultTemplate . "/footer.php";
}
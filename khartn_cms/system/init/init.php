<?php

if (Config::$session_handler != "") {
// setting up session cache
    include_once $_SERVER["DOCUMENT_ROOT"] . "/khartn_cms/system/cache/session/" . Config::$session_handler . "/sessionHandler.php";
}

// setting up content cache
include_once $_SERVER["DOCUMENT_ROOT"] . "/khartn_cms/system/cache/content/" . Config::$cacheType . "/Cache.php";

// setting up request processor
include_once $_SERVER["DOCUMENT_ROOT"] . "/khartn_cms/system/requests/requests_processor.php";

// setting up main APPLICATION class
include_once $_SERVER["DOCUMENT_ROOT"] . "/khartn_cms/system/application/components.php";
include_once $_SERVER["DOCUMENT_ROOT"] . "/khartn_cms/system/application/modules.php";
include_once $_SERVER["DOCUMENT_ROOT"] . "/khartn_cms/system/application/meta_tags_handler.php";
include_once $_SERVER["DOCUMENT_ROOT"] . "/khartn_cms/system/application/application.php";
APPLICATION::init();
APPLICATION::setTitle(Config::$defaultTitle);

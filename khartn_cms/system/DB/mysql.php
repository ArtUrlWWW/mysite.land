<?php

/**
 * Description of db_init
 *
 * @author wwwdev
 */
class DB {

    private static $MYSQL_HOST = '';
    private static $MYSQL_DATABASE = '';
    private static $MYSQL_USER = '';
    private static $MYSQL_PASSWORD = '';
    protected static $_instance;
    protected static $_pConnectIndicator = false;
    protected static $_connect = false;

    public function escapeStr($strData) {
        return mysql_real_escape_string($strData);
    }

    public static function connect() {
        if (!self::$_connect) {
            if (self::$_pConnectIndicator) {
                self::$_connect = @mysql_pconnect(self::$MYSQL_HOST, self::$MYSQL_USER, self::$MYSQL_PASSWORD) or die('Не удалось соединиться: ' . mysql_error());
            } else {
                self::$_connect = mysql_connect(self::$MYSQL_HOST, self::$MYSQL_USER, self::$MYSQL_PASSWORD) or die('Не удалось соединиться: ' . mysql_error());
            }
            mysql_select_db(self::$MYSQL_DATABASE) or die('Не удалось выбрать базу данных');
            mysql_set_charset("utf8");
        }
    }

    public static function query($queryString) {
        return mysql_query($queryString, self::$_connect);
    }

    public static function getLastInsertID() {
        return mysql_insert_id(self::$_connect);
    }

    private function __construct() {
        
    }

    private function __clone() {
        
    }

    /**
     * 
     * @return DB
     */
    public static function getInstance() {
        // проверяем актуальность экземпляра
        if (null === self::$_instance) {
            // создаем новый экземпляр
            self::$_instance = new self();
        }
        // возвращаем созданный или существующий экземпляр
        return self::$_instance;
    }

    public static function set_pConnectIndicator($pConnectIndicator) {
        self::$_pConnectIndicator = $pConnectIndicator;
    }

    public static function setMySQLHost($host) {
        self::$MYSQL_HOST = $host;
    }

    public static function setMySQLDB($dbName) {
        self::$MYSQL_DATABASE = $dbName;
    }

    public static function setMySQLUser($userName) {
        self::$MYSQL_USER = $userName;
    }

    public static function setMySQLPass($passwd) {
        self::$MYSQL_PASSWORD = $passwd;
    }

    public static function getMySQLHost() {
        return self::$MYSQL_HOST;
    }

    public static function getMySQLDB() {
        return self::$MYSQL_DATABASE;
    }

    public static function getMySQLUser() {
        return self::$MYSQL_USER;
    }

    public static function getMySQLPass() {
        return self::$MYSQL_PASSWORD;
    }

}

<?php

/**
 * Description of config
 *
 * @author wwwdev
 */
class Config {
    static $db_config_handler_class;
    static $session_handler;
    static $cacheType;
    static $defaultTemplate;
    static $defaultTitle;
    static $checkCaptcha;
    static $recaptchaPublicKey;
    static $recaptchaPrivateKey;
    static $defaulSiteLang;
}

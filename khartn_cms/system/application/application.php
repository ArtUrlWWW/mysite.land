<?php

class APPLICATION {

    /**
     *
     * @var Memcache
     */
    static $cache;

    /**
     *
     * @var Core_requests
     */
    static $requestsProcessor;

    /**
     *
     * @var MetaTagsHandler
     */
    static $metaTagsHandler;

    /**
     * @var Modules
     */
    static $modules;

    /**
     * Включать ли в вывод шаблон или выводить только вывод от компонентов.
     * @var boolean
     */
    static $showCleanOutput = false;

    public static function init() {
        self::$cache = Cache::getInstance();
        self::$requestsProcessor = Core_requests::getInstance();
        self::$metaTagsHandler = MetaTagsHandler::getInstance();
        self::$modules = Modules::getInstance();

        if (self::$requestsProcessor->lang == "") {
            self::$requestsProcessor->get_vars('lang/page');
            if (self::$requestsProcessor->lang == "/") {
                self::$requestsProcessor->lang = Config::$defaulSiteLang;
            }
        }
        self::setDictionary();
    }

    private static function setDictionary() {

        switch (self::$requestsProcessor->lang) {
            case "ru":
                include_once $_SERVER["DOCUMENT_ROOT"] . '/khartn_cms/translations/RussianDictionary.php';

                break;
            case "en":
                include_once $_SERVER["DOCUMENT_ROOT"] . '/khartn_cms/translations/EnglishDictionary.php';

                break;
            case "eng":
                include_once $_SERVER["DOCUMENT_ROOT"] . '/khartn_cms/translations/EnglishDictionary.php';

                break;

            default:
                self::$requestsProcessor->lang = "ru";
                include_once $_SERVER["DOCUMENT_ROOT"] . '/khartn_cms/translations/RussianDictionary.php';
                break;
        }
    }

    public static function setShowCleanOutput($showCleanOutput) {
        self::$showCleanOutput = $showCleanOutput;
    }

    public static function includeComponent($componentName, $template = "default") {
        Components::getInstance()->includeComponent($componentName, $template);
    }

    public static function addJS($jsURL) {
        self::$metaTagsHandler->addJS($jsURL);
    }

    public static function printJSURLs() {
        self::$metaTagsHandler->printJSURLs();
    }

    public static function addCSS($cssURL) {
        self::$metaTagsHandler->addCSS($cssURL);
    }

    public static function printCSSURLs() {
        self::$metaTagsHandler->printCSSURLs();
    }

    public static function setTitle($title) {
        self::$metaTagsHandler->setTitle($title);
    }

    public static function printTitle() {
        self::$metaTagsHandler->printTitle();
    }

    public static function redirectToPage($url) {
        header('Location: ' . $url);
    }

    public static function includeModule($moduleName) {
        self::$modules->includeModule($moduleName);
    }

    public static function get_client_ip() {
        $ipaddress = '';
        if ($_SERVER['HTTP_CLIENT_IP'])
            $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
        else if ($_SERVER['HTTP_X_FORWARDED_FOR'])
            $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
        else if ($_SERVER['HTTP_X_FORWARDED'])
            $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
        else if ($_SERVER['HTTP_FORWARDED_FOR'])
            $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
        else if ($_SERVER['HTTP_FORWARDED'])
            $ipaddress = $_SERVER['HTTP_FORWARDED'];
        else if ($_SERVER['REMOTE_ADDR'])
            $ipaddress = $_SERVER['REMOTE_ADDR'];
        else
            $ipaddress = 'UNKNOWN';

        return $ipaddress;
    }
    
    public static function templateDir() {
        return '/khartn_cms/templates/'.Config::$defaultTemplate."/"; 
    }

}

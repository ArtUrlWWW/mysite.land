<?php

/**
 * Description of components
 *
 * @author wwwdev
 */
class Components {

    protected static $_instance;

    private function __construct() {
        
    }

    private function __clone() {
        
    }

    /**
     * 
     * @return Components
     */
    public static function getInstance() {
        // проверяем актуальность экземпляра
        if (null === self::$_instance) {
            // создаем новый экземпляр
            self::$_instance = new self();
        }
        // возвращаем созданный или существующий экземпляр
        return self::$_instance;
    }

    public static function includeComponent($componentName, $template = "default") {
        $componentName = split(":", $componentName);
        if (file_exists($_SERVER["DOCUMENT_ROOT"] . '/khartn_cms/components/' . $componentName[0] . '/' . $componentName[1] . '/component.php')) {
            include_once $_SERVER["DOCUMENT_ROOT"] . '/khartn_cms/components/' . $componentName[0] . '/' . $componentName[1] . '/component.php';

            if (file_exists( $_SERVER["DOCUMENT_ROOT"] . '/khartn_cms/components/' . $componentName[0] . '/' . $componentName[1] . '/template/' . $template . '/styles.css')) {
                APPLICATION::$metaTagsHandler->addCSS('/khartn_cms/components/' . $componentName[0] . '/' . $componentName[1] . '/template/' . $template . '/styles.css');
            }
            
            if (file_exists( $_SERVER["DOCUMENT_ROOT"] . '/khartn_cms/components/' . $componentName[0] . '/' . $componentName[1] . '/template/' . $template . '/scripts.js')) {
                APPLICATION::$metaTagsHandler->addJS('/khartn_cms/components/' . $componentName[0] . '/' . $componentName[1] . '/template/' . $template . '/scripts.js');
            }

            include_once $_SERVER["DOCUMENT_ROOT"] . '/khartn_cms/components/' . $componentName[0] . '/' . $componentName[1] . '/template/' . $template . '/template.php';
        } else {
            echo "Компонент не найден.";
        }
    }

}

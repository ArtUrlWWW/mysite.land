<?php

/**
 * Description of components
 *
 * @author wwwdev
 */
class Modules {

    protected static $_instance;

    private function __construct() {
        
    }

    private function __clone() {
        
    }

    /**
     * 
     * @return Modules
     */
    public static function getInstance() {
        // проверяем актуальность экземпляра
        if (null === self::$_instance) {
            // создаем новый экземпляр
            self::$_instance = new self();
        }
        // возвращаем созданный или существующий экземпляр
        return self::$_instance;
    }

    public static function includeModule($moduleName) {
        $moduleName = split(":", $moduleName);
        include_once $_SERVER["DOCUMENT_ROOT"] . '/khartn_cms/modules/' . $moduleName[0] . '/' . $moduleName[1] . '/index.php';
    }

}

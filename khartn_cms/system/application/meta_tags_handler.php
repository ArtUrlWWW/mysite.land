<?php

/**
 * Description of meta_tags_handler
 *
 * @author wwwdev
 */
class MetaTagsHandler {

    protected static $_instance;
    private static $cssURLArray;
    private static $jsURLArray;
    private static $title;

    private function __construct() {
        self::$jsURLArray = array();
    }

    private function __clone() {
        
    }

    /**
     * 
     * @return MetaTagsHandler
     */
    public static function getInstance() {
        // проверяем актуальность экземпляра
        if (null === self::$_instance) {
            // создаем новый экземпляр
            self::$_instance = new self();
        }
        // возвращаем созданный или существующий экземпляр
        return self::$_instance;
    }

    public static function addJS($jsURL) {
        self::$jsURLArray[] = $jsURL;
    }

    public static function printJSURLs() {
        foreach (self::$jsURLArray as $key => $value) {
            ?>
            <script type="text/javascript"  src="<?= $value ?>"></script>
            <?php
        }
    }

    public static function addCSS($cssURL) {
        self::$cssURLArray[] = $cssURL;
    }

    public static function printCSSURLs() {
        foreach (self::$cssURLArray as $key => $value) {
            ?>
            <link href="<?= $value ?>" rel="stylesheet" />
            <?php
        }
    }

    public static function setTitle($title) {
        self::$title = $title;
    }

    public static function printTitle() {
        ?>
        <title><?= self::$title ?></title>
        <?php
    }

}

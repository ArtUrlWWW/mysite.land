<nav>
    <ul>

        <?php
        $requestsProcessor = Core_requests::getInstance();
        $pathToURL = $requestsProcessor->page;
//        echo "!!!".$pathToURL."!!!";

        $homeSelector = "";
        if ($pathToURL == '/') {
            $homeSelector = "selected";
        }
        ?>
        <li class="<?= $homeSelector ?>">
            <a class="up <?= $homeSelector ?>" href="/<?= APPLICATION::$requestsProcessor->lang ?>/">
                <?php
                if ($_SESSION['uname'] == "") {

                    echo Dict::$HOME;
                } else {
                    echo Dict::$landings;
                }
                ?>
            </a>
        </li>


        <li>
            <a class="up" href="/<?= APPLICATION::$requestsProcessor->lang ?>/">
                <?= Dict::$PRICING ?>
            </a>
        </li>
        <li>
            <a class="up" href="/<?= APPLICATION::$requestsProcessor->lang ?>/">
                <?= Dict::$BLOG ?>
            </a>
        </li>
        <li>
            <a class="up" href="/<?= APPLICATION::$requestsProcessor->lang ?>/">
                <?= Dict::$CONTACTS ?>
            </a>
        </li>
        <?php
        if ($_SESSION['uname'] != "") {
            ?>
            <li class="<?= $accountSelector ?>">
                <a class="up <?= $accountSelector ?>" href="/<?= APPLICATION::$requestsProcessor->lang ?>/account">
                    <?= Dict::$account ?>
                </a>
            </li>
            <?php
        } else {
            $loginSelector = "";
            if ($pathToURL == 'login') {
                $loginSelector = "selected";
            }
            ?>
            <li class="<?= $loginSelector ?>">
                <a class="up <?= $loginSelector ?>" href="/<?= APPLICATION::$requestsProcessor->lang ?>/login">
                    <?= Dict::$LOG_IN ?>
                </a>
            </li>
            <?php
        }
        ?>

    </ul>
</nav>
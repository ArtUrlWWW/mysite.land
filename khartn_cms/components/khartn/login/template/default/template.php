<?php
APPLICATION::$metaTagsHandler->addCSS("/khartn_cms/templates/land/css/login.css");
?>

<?php
if ($errorStr != null) {
    ?>
    <div class="login-form-error">
        <?= $errorStr ?>
    </div>
    <?php
}
?>

<div class="login-form">
    <form method="POST">
        <div class="header1">
            <?= Dict::$LOG_IN ?>
        </div>
        <div class="stroka">
            <input class="stroka-input" value="<?= $email ?>" type="text" name="email" placeholder="Email">
        </div>
        <div class="stroka">
            <input class="stroka-input" value="<?= $passwd ?>" type="password" name="passwd" placeholder="Password">
        </div>
        <?php
        if (Config::$checkCaptcha) {
            ?>
            <div class="captcha-wrapper" >
                <?= recaptcha_get_html($publickey, $error); ?>
            </div>        
            <?php
        }
        ?>
        <div class="stroka" style="margin-top: 15px; text-align: center;">
            <button class="redButton">
                <?= Dict::$LOG_IN ?>
            </button>
        </div>
        <div class="stroka">
            <div class="regLink">
                <a href="/<?= APPLICATION::$requestsProcessor->lang ?>/reg"><?= Dict::$register ?></a>
            </div>
        </div>
    </form>
</div>
<?php

require_once($_SERVER["DOCUMENT_ROOT"] . '/khartn_cms/third-party-libs/recaptcha-php-1.11/recaptchalib.php');
// Get a key from https://www.google.com/recaptcha/admin/create
$publickey = Config::$recaptchaPublicKey;
$privatekey = Config::$recaptchaPrivateKey;

# the response from reCAPTCHA
$resp = null;
# the error code from reCAPTCHA, if any
$error = null;
# was there a reCAPTCHA response?

$errorStr = null;

if ($_SESSION['uname'] != "") {
    APPLICATION::redirectToPage("/" . APPLICATION::$requestsProcessor->lang . "/");
}

if (count($_POST) > 0) {

    if (Config::$checkCaptcha) {
        if ($_POST["recaptcha_response_field"]) {
            $resp = recaptcha_check_answer($privatekey, $_SERVER["REMOTE_ADDR"], $_POST["recaptcha_challenge_field"], $_POST["recaptcha_response_field"]);

            if (!$resp->is_valid) {
                # set the error code so that we can display it
                $errorStr = Dict::$wrongCaptcha;
            }
        } else {
            $errorStr = Dict::$wrongCaptcha;
        }
    }

    $email = filter_input(INPUT_POST, 'email', FILTER_VALIDATE_EMAIL);
    $passwd = filter_input(INPUT_POST, 'passwd', FILTER_SANITIZE_FULL_SPECIAL_CHARS);

    $uip = APPLICATION::get_client_ip();

    if ($errorStr == null) {
        APPLICATION::includeModule("khartn:kuser");
        $auth = Auth::instance();
                
        if ($auth->login($email, $passwd, $uip)) {
            $_SESSION['uname'] = $email;
            $_SESSION['uID'] = $auth::$user->getUID();
            APPLICATION::redirectToPage("/" . APPLICATION::$requestsProcessor->lang . "/");
        } else {
            $errorStr = Dict::$authWrongLoginOrPassword;
        }
    }
}


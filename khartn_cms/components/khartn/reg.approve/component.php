<?php

APPLICATION::includeModule("khartn:kuser");

require_once($_SERVER["DOCUMENT_ROOT"] . '/khartn_cms/third-party-libs/recaptcha-php-1.11/recaptchalib.php');
// Get a key from https://www.google.com/recaptcha/admin/create
$publickey = "6Lc1VOwSAAAAAJJulJA6x6XWdm2NF2URehB9l-9V";
$privatekey = "6Lc1VOwSAAAAAIIYzM2kmxSUmLHsledPCLW3hb7D";

# the response from reCAPTCHA
$resp = null;
# the error code from reCAPTCHA, if any
$error = null;
# was there a reCAPTCHA response?

$error="";

if ($_POST["recaptcha_response_field"]) {
    $resp = recaptcha_check_answer($privatekey, $_SERVER["REMOTE_ADDR"], $_POST["recaptcha_challenge_field"], $_POST["recaptcha_response_field"]);

    if ($resp->is_valid) {
        
    } else {
        # set the error code so that we can display it
        $error=  Dict::$wrongCaptcha;
    }
}

$user = new KUser();
$user->setUserName("USER");
$user->setRegIP("127.0.0.1");
$user->setPhone("+7.........");
$user->setPasswd("PASSWD");
$user->setCheckSMSCode("SMSCODE");
$user->add();


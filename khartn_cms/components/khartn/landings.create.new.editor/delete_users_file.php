<?php

//ini_set("display_errors", 1);
include_once $_SERVER["DOCUMENT_ROOT"] . '/khartn_cms/system/init/header.php';

APPLICATION::includeModule("khartn:kuser");
/**
 * @var Auth
 */
Auth::instance();

if (Auth::$user->getUID() != "") {

    APPLICATION::includeModule("land:landings");
    APPLICATION::includeModule("khartn:kfile");

    $fileIDs = filter_input(INPUT_POST, 'fileId', FILTER_SANITIZE_FULL_SPECIAL_CHARS);
    
//    echo "fileIDs 1 ".$fileIDs."\r\n" ;

    $checkOwner = false;
    $checkOwner = KLandings::checkFileOwner($fileIDs, Auth::$user->getUID());

    if ($checkOwner) {
        $kfile = new KFile();
        
        $kfile->deleteFile($fileIDs);
        KLandings::deleteLandingsFile($fileIDs, Auth::$user->getUID());
    } else {
        exit_status("not_file_owner");
    }
} else {
    exit_status("not_auth");
}

exit_status('something_wrong');

// Helper functions
function exit_status($str) {
    echo json_encode(array('status' => $str));
    exit;
}

?>

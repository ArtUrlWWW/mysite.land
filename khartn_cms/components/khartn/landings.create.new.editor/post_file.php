<?php

//ini_set("display_errors", 1);
include_once $_SERVER["DOCUMENT_ROOT"] . '/khartn_cms/system/init/header.php';

APPLICATION::includeModule("khartn:kuser");
/**
 * @var Auth
 */
Auth::instance();

//echo "!!!\r\n";
//print_r(Auth::$user);
//echo "!!!\r\n";

if (Auth::$user->getUID() != "") {

    $allowed_ext = array('jpg', 'jpeg', 'png', 'gif');

    if (strtolower($_SERVER['REQUEST_METHOD']) != 'post') {
        exit_status('wrong_HTTP_method');
    }


    if (array_key_exists('pic', $_FILES) && $_FILES['pic']['error'] == 0
             && $_FILES['pic']['size']<=1572864) {

        $pic = $_FILES['pic'];

        if (!in_array(get_extension($pic['name']), $allowed_ext)) {
//        exit_status('Only ' . implode(',', $allowed_ext) . ' files are allowed!');
            exit_status('bad_ext');
        }

        APPLICATION::includeModule("khartn:kfile");
        APPLICATION::includeModule("land:landings");

        /**
         * KFile
         */
        $kfile = new KFile();
        $file = $kfile->fromUpload($pic, "landings");
        exit_status($file->getFilePath());
        $file = KLandings::insertNewLandingFileToDB($file);

//        print_r($file);

        if ($file) {
            exit_status($file);
        } else {
            exit_status("error_on_upload");
        }
    } else {
        exit_status("file_too_big");
    }
} else {
    exit_status("not_auth");
}

exit_status('something_wrong');

// Helper functions
function exit_status($str) {
    echo json_encode(array('status' => $str));
    exit;
}

function get_extension($file_name) {
    $ext = explode('.', $file_name);
    $ext = array_pop($ext);
    return strtolower($ext);
}

?>

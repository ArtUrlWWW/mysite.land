<?php

ini_set("display_errors", 1);
include_once $_SERVER["DOCUMENT_ROOT"] . '/khartn_cms/system/init/header.php';

//print_r($_POST);

APPLICATION::includeModule("khartn:kuser");
/**
 * @var Auth
 */
Auth::instance();

if (Auth::$user->getUID() != "") {

    APPLICATION::includeModule("land:landings");

    $stylesMainContent = $_POST['stylesMainContent'];

    $cssNameFull = filter_input(INPUT_POST, 'cssNameFull', FILTER_SANITIZE_FULL_SPECIAL_CHARS);

    $dir = $_SERVER["DOCUMENT_ROOT"] . substr($cssNameFull, 0, strrpos($cssNameFull, "/"));
    mkdir($dir, 0777, true);
    $cssFile = $_SERVER["DOCUMENT_ROOT"] . $cssNameFull;
    file_put_contents($cssFile, $stylesMainContent);

    $cssName = filter_input(INPUT_POST, 'cssName', FILTER_SANITIZE_FULL_SPECIAL_CHARS);
//        $pageContent = filter_input(INPUT_POST, 'pageContent', FILTER_SANITIZE_FULL_SPECIAL_CHARS);
    $pageContent = $_POST['pageContent'];
    @mkdir($_SERVER["DOCUMENT_ROOT"] . "/uploads/landings-pages/", 0777, true);
    $bodyFile = $_SERVER["DOCUMENT_ROOT"] . "/uploads/landings-pages/" . $cssName . ".html";
    file_put_contents($bodyFile, $pageContent);

    $klanding = new KLanding($cssFile, $bodyFile);

    $landingNameGlobal = filter_input(INPUT_POST, 'landingNameGlobal', FILTER_SANITIZE_FULL_SPECIAL_CHARS);

    if ($landingNameGlobal == "") {
        $klanding->save();
    } else {
        $klanding->setName($landingNameGlobal);
        $klanding->update();
    }
} else {
    exit_status("not_auth");
}

exit_status('OK');

// Helper functions
function exit_status($str) {
    echo json_encode(array('status' => $str));
    exit;
}

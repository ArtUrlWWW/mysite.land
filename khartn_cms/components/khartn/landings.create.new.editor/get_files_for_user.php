<?php

//ini_set("display_errors", 1);
include_once $_SERVER["DOCUMENT_ROOT"] . '/khartn_cms/system/init/header.php';

APPLICATION::includeModule("khartn:kuser");
/**
 * @var Auth
 */
Auth::instance();

if (Auth::$user->getUID() != "") {

    APPLICATION::includeModule("land:landings");
    APPLICATION::includeModule("khartn:kfile");

    $ids = KLandings::getLandingsFilesIDsByUserID(Auth::$user->getUID());

    $kfile = new KFile();
    $files = $kfile->getByIDList(implode(",", $ids), false);
    
    exit_status($files);
} else {
    exit_status("not_auth");
}

exit_status('something_wrong');

// Helper functions
function exit_status($str) {
    echo json_encode(array('status' => $str));
    exit;
}

?>

<?php

APPLICATION::includeModule("khartn:kuser");

require_once($_SERVER["DOCUMENT_ROOT"] . '/khartn_cms/third-party-libs/recaptcha-php-1.11/recaptchalib.php');
// Get a key from https://www.google.com/recaptcha/admin/create
$publickey = Config::$recaptchaPublicKey;
$privatekey = Config::$recaptchaPrivateKey;

# the response from reCAPTCHA
$resp = null;
# the error code from reCAPTCHA, if any
$error = null;
# was there a reCAPTCHA response?

$errorStr = null;

if (count($_POST) > 0) {


    if (Config::$checkCaptcha) {
        if ($_POST["recaptcha_response_field"]) {
            $resp = recaptcha_check_answer($privatekey, $_SERVER["REMOTE_ADDR"], $_POST["recaptcha_challenge_field"], $_POST["recaptcha_response_field"]);

            if (!$resp->is_valid) {
                # set the error code so that we can display it
                $errorStr = Dict::$wrongCaptcha;
            }
        } else {
            $errorStr = Dict::$wrongCaptcha;
        }
    }

    $email = filter_input(INPUT_POST, 'email', FILTER_VALIDATE_EMAIL);

    if (!$email) {
        $errorStr = Dict::$wrongEmailFormat;
    }

    $passwd = filter_input(INPUT_POST, 'passwd', FILTER_SANITIZE_FULL_SPECIAL_CHARS);

    if (strlen($passwd) < 7) {
        $errorStr = Dict::$incorrectPasswd;
    }

    $uip = APPLICATION::get_client_ip();

    $phone = filter_input(INPUT_POST, 'phone', FILTER_SANITIZE_FULL_SPECIAL_CHARS);
    $phone = str_replace(array("+", " ", "-", ")", "("), array("", "", "", "", ""), $phone);

    if (strlen($phone) < 5) {
        $errorStr = Dict::$incorrectPhone;
    }

    for ($x = 1; $x < 11; $x++) {
        $SMSCODE = mt_rand(1847, 15834);
    }

    if ($errorStr == null) {

        $user = new KUser();
        $user->setUserName($email);
        $user->setRegIP($uip);
        $user->setPhone($phone);
        $user->setPasswd($passwd);
        $user->setCheckSMSCode($SMSCODE);
        if (!$user->add()) {
            $errorStr = Dict::$thisEmailIsAlreadyRegistered;
        }
    }

    if ($errorStr == null) {
        APPLICATION::redirectToPage("/" . APPLICATION::$requestsProcessor->lang . "/login");
//        APPLICATION::redirectToPage("/" . APPLICATION::$requestsProcessor->lang . "/approve");
    }
}

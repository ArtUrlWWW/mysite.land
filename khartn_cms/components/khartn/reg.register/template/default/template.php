<?php
APPLICATION::$metaTagsHandler->addCSS("/khartn_cms/templates/land/css/login.css");
?>

<?php
if ($errorStr != null) {
    ?>
    <div class="login-form-error">
        <?= $errorStr ?>
    </div>
    <?php
}
?>

<div class="login-form">
    <form method="POST">
        <div class="header1">
            <?= Dict::$probaPytnadcatDnye ?>
        </div>
        <div class="stroka">
            <input class="stroka-input" type="text" value="<?= $email ?>" name="email" placeholder='<?= Dict::$emailVariant1; ?>'>
        </div>
        <div class="stroka">
            <input class="stroka-input" type="password" value="<?= $passwd ?>" name="passwd" placeholder="<?= Dict::$passwdVariant1 ?>">
        </div>
        <div class="stroka">
            <input class="stroka-input" type="text" value="<?= $phone ?>" name="phone" placeholder="<?= Dict::$phoneVariant1 ?>">
        </div>
        <?php
        if (Config::$checkCaptcha) {
            ?>
            <div class="captcha-wrapper" >
                <?= recaptcha_get_html($publickey, $error); ?>
            </div>
            <?php
        }
        ?>

        <div class="stroka">
            <button class="redButton">
                Регистрация
            </button>
        </div>
    </form>
</div>
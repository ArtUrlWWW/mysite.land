<div class="landingsList">
    <div class="buttonHolder">
        <a href="/<?= APPLICATION::$requestsProcessor->lang ?>/new/" class="redButton" id="createNewLandingPage">
            <?= Dict::$createNewLandingPage ?>&nbsp;<i class="icon-plus-circle"></i>
        </a>
    </div>   

    <?php
    $klanding = new KLanding();
    /**
     * @var KLanding[] Description
     */
    foreach ($klanding->getByOwnerID(Auth::$user->getUID()) as $key => $klanding) {
        ?>
        <div class="landing">
            <div class="landing-id-div">
                <?= Dict::$landingsID . ": " . $klanding->getID() ?>
            </div>
            <div class="link-to-landing">
                <a href="http://<?= $klanding->getName() . ".landings.pw" ?>" target="_blank">
                    http://<?= $klanding->getName() . ".landings.pw" ?>
                </a>
            </div>

            <div class="edit-button-holder">
                <a href="/<?= APPLICATION::$requestsProcessor->lang ?>/editor/<?= $klanding->getID() ?>">
                    <?= Dict::$edit ?>
                </a>
            </div>

        </div>

        <?php
    }
    ?>

</div>

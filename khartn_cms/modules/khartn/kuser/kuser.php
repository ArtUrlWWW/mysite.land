<?php

/**
 * Description of KUser
 *
 * @author wwwdev
 */
class KUser {

    private $uid;
    private $username;
    private $passwd;
    private $phone;
    private $regIP;
    private $checkSMSCode;
    private $surname;
    private $realName;
    private $patronymic;

    /**
     * 
     * @param type $userID
     * @return KUser
     */
    public function getByID($userID) {

        $query = "select * from k_users where id = " . DB::getInstance()->escapeStr($userID) . ";";
        
//                echo "\r\n" . $query . "\r\n";
        
        $result = DB::query($query);
        $array = false;
        while ($array = mysql_fetch_array($result)) {
            $this->setCheckSMSCode($array['checkSMSCode']);
            $this->setPasswd($array['passwd']);
            $this->setPhone($array['phone']);
            $this->setRegIP($array['reg_ip']);
            $this->setUserName($array['username']);
            $this->setUID($array['id']);
            $this->setUserSurname($array['surname']);
            $this->setUserRealName($array['name']);
            $this->setUserPatronymic($array['patronymic']);
            return $this;
        }
        return FALSE;
    }
    
    /**
     * 
     * @param type $email
     * @param type $passwd
     * @return KUser
     */
    public function get($email, $passwd) {

        $query = "select * from k_users where username like '" . DB::getInstance()->escapeStr($email) . "' "
                . " and passwd like PASSWORD('" . DB::getInstance()->escapeStr($passwd) . "');";
        
        //        echo "\r\n" . $query . "\r\n";
        
        $result = DB::query($query);
        $array = false;
        while ($array = mysql_fetch_array($result)) {
            $kuser = new self();
            $kuser->setCheckSMSCode($array['checkSMSCode']);
            $kuser->setPasswd($array['passwd']);
            $kuser->setPhone($array['phone']);
            $kuser->setRegIP($array['reg_ip']);
            $kuser->setUserName($array['username']);
            $kuser->setUID($array['id']);
            $kuser->setUserSurname($array['surname']);
            $kuser->setUserRealName($array['name']);
            $kuser->setUserPatronymic($array['patronymic']);
            return $kuser;
        }
        return FALSE;
    }

    public function add() {

        $query = "select id from k_users where username like '" . DB::getInstance()->escapeStr($this->username) . "';";
        
        //        echo "\r\n" . $query . "\r\n";
        
        $result = DB::query($query);
        $hasData = false;
        while ($line = mysql_fetch_array($result)) {
            $hasData = true;
        }

        if (!$hasData) {
            $query = "insert into k_users(username, passwd, phone, reg_ip, checkSMSCode,"
                    . " surname, name, patronymic)"
                    . " values('" . DB::getInstance()->escapeStr($this->username)
                    . "', PASSWORD('" . DB::getInstance()->escapeStr($this->passwd) . "'), "
                    . "'" . DB::getInstance()->escapeStr($this->phone) . "', "
                    . "'" . DB::getInstance()->escapeStr($this->regIP) . "', "
                    . "'" . DB::getInstance()->escapeStr($this->checkSMSCode) . "', "
                    . "'" . DB::getInstance()->escapeStr($this->surname) . "', "
                    . "'" . DB::getInstance()->escapeStr($this->realName) . "', "
                    . "'" . DB::getInstance()->escapeStr($this->patronymic) . "' "
                    . " );";

//        echo "\r\n" . $query . "\r\n";
            DB::query($query);

            return true;
        }
        return false;
    }

    public function setUserName($userName) {
        $this->username = $userName;
    }

    public function setPasswd($passwd) {
        $this->passwd = $passwd;
    }

    public function setPhone($phone) {
        $this->phone = $phone;
    }

    public function setRegIP($ip) {
        $this->regIP = $ip;
    }

    public function setUID($UID) {
        $this->uid = $UID;
    }

    public function setCheckSMSCode($checkSMSCode) {
        $this->checkSMSCode = $checkSMSCode;
    }

    public function setUserSurname($surname) {
        $this->surname = $surname;
    }

    public function setUserRealName($username) {
        $this->realName = $username;
    }

    public function setUserPatronymic($patronymic) {
        $this->patronymic = $patronymic;
    }

    public function getUID() {
        return $this->uid;
    }

    public function getUserName() {
        return $this->username;
    }

    public function getPasswd() {
        return $this->passwd;
    }

    public function getPhone() {
        return $this->phone;
    }

    public function getRegIP() {
        return $this->regIP;
    }

    public function getCheckSMSCode() {
        return $this->regIP;
    }

    public function getUserSurname() {
        return $this->surname;
    }

    public function getUserRealName() {
        return $this->realName;
    }

    public function getUserPatronymic() {
        return $this->patronymic;
    }

}

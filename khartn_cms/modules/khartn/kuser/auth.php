<?php

final class Auth {

    /**
     *
     * @var KUser
     */
    public static $user;

    /**
     * 
     * @staticvar boolean $self
     * @return Auth
     */
    public static function instance() {
        static $self = false;
        if (false === $self) {
            $self = new self();
        }

        return $self;
    }

    private function __construct() {
        
        self::$user = new KUser();
        self::$user->setUID($_SESSION['uID']);
        self::$user->setUserName($_SESSION['uname']);
    }

    public function login($email, $passwd, $uip) {
        self::$user = self::$user->get($email, $passwd);

        if (!self::$user) {
            return false;
        } else {
            $query = "insert into k_logins(uid, auth_ip)"
                    . " values("
                    . DB::getInstance()->escapeStr(self::$user->getUID())
                    . ", '" . DB::getInstance()->escapeStr($uip) . "'"
                    . " );";

            DB::query($query);

            return true;
        }
    }

}

?>

<?php

/**
 * Description of KFile
 *
 * @author wwwdev
 */
class KFile {

    public $file_id;
    public $file_name;
    public $file_path;
    public $file_type;
    public $file_size;
    public $file_add_date;
    public $file_add_ip;
    public $file_document_root;

    public function getFileId() {
        return $this->file_id;
    }

    public function getFileName() {
        return $this->file_name;
    }

    public function getFilePath() {
        return $this->file_path;
    }

    public function getFileType() {
        return $this->file_type;
    }

    public function getFileSize() {
        return $this->file_size;
    }

    public function getFileAddDate() {
        return $this->file_add_date;
    }

    public function getFileAddIp() {
        return $this->file_add_ip;
    }

    public function getFileDocumentRoot() {
        return $this->file_document_root;
    }

    public function setFileId($file_id) {
        $this->file_id = $file_id;
    }

    public function setFileName($file_name) {
        $this->file_name = $file_name;
    }

    public function setFilePath($file_path) {
        $this->file_path = $file_path;
    }

    public function setFileType($file_type) {
        return $this->file_type = $file_type;
    }

    public function setFileSize($file_size) {
        return $this->file_size = $file_size;
    }

    public function setFileAddDate($file_add_date) {
        return $this->file_add_date = $file_add_date;
    }

    public function setFileAddIp($file_add_ip) {
        return $this->file_add_ip = $file_add_ip;
    }

    public function setFileDocumentRoot($file_document_root) {
        $this->file_document_root = $file_document_root;
    }

    public function fromUpload($arrayFromFILES, $basedir = false, $specificDir = false, $specificFileName = FALSE) {

        $uploadDir = $_SERVER["DOCUMENT_ROOT"] . '/uploads/';

        if ($basedir) {
            $uploadDir.=$basedir . "/";
        }

        if (!$specificDir) {
            $specificDir = $this->getNameBasedOnTime();
        }

        $uploadDir = $uploadDir . $specificDir . "/";
        mkdir($uploadDir, 0777, true);

        if (!$specificFileName) {
            do {
                $specificFileName = uniqid() . $arrayFromFILES['name'];
            } while (file_exists($uploadDir . $specificFileName));
        }
        $specificFileName = $uploadDir . $specificFileName;

        if (move_uploaded_file($arrayFromFILES['tmp_name'], $specificFileName)) {

            $kfileLocal = new KFile();

            $kfileLocal->setFileName($arrayFromFILES['name']);
            $kfileLocal->setFileType($arrayFromFILES['type']);
            $kfileLocal->setFileSize($arrayFromFILES['size']);
            $kfileLocal->setFilePath(str_replace($_SERVER["DOCUMENT_ROOT"], "", $specificFileName));
            $kfileLocal->setFileDocumentRoot($_SERVER["DOCUMENT_ROOT"]);

            $kfileLocal = $this->saveToDB($kfileLocal);

            return $kfileLocal;
        } else {
            return false;
        }
    }

    /**
     * 
     * @param KFile $kfile
     */
    public function saveToDB($kfile) {

        $query = "insert into k_files(file_name, file_path, file_type, file_size, file_add_ip, document_root, file_owner) values ("
                . "'" . DB::getInstance()->escapeStr($kfile->getFileName()) . "', "
                . "'" . DB::getInstance()->escapeStr($kfile->getFilePath()) . "', "
                . "'" . DB::getInstance()->escapeStr($kfile->getFileType()) . "', "
                . "'" . DB::getInstance()->escapeStr($kfile->getFileSize()) . "', "
                . "'" . DB::getInstance()->escapeStr(APPLICATION::get_client_ip()) . "', "
                . "'" . DB::getInstance()->escapeStr($kfile->getFileDocumentRoot()) . "', "
                . "'" . DB::getInstance()->escapeStr(Auth::$user->getUID()) . "' "
                . ");";

//        echo "\r\n" . $query . "\r\n";

        DB::query($query);
        $kfile->setFileId(DB::getLastInsertID());
        return $kfile;
    }

    private function getUniqueName() {

        $specificDir = "";
        do {
            $specificDir = uniqid();
        } while (file_exists($uploadDir . $specificDir));
        return $specificDir;
    }

    private function getNameBasedOnTime() {
        $time = round(microtime(-1));
        $time = substr($time, 0, strlen($time) - 5) . "0000";
        return $time;
    }

    /**
     * Возращает список объектов KFile по ID файлов
     * @param string Строка вида '1,2,3....'     
     */
    function getByIDList($idList, $allFiles = true) {
        if ($allFiles) {
            $query = "select * from k_files where id in (" . DB::getInstance()->escapeStr($idList) . "); ";
//        echo "\r\n" . $query . "\r\n";
        } else {
            $query = "select * from k_files where id in (" . DB::getInstance()->escapeStr($idList) . ") "
                    . " and file_type not in ('css', 'htmlbody'); ";
//        echo "\r\n" . $query . "\r\n";
        }

        $result = DB::query($query);
        $array = false;

        $outArr = array();

        while ($array = mysql_fetch_array($result)) {
            /**
             * @var KFile
             */
            $kfile = new KFile();

            $kfile->setFileId($array['id']);
            $kfile->setFileName($array['file_name']);
            $kfile->setFilePath($array['file_path']);
            $kfile->setFileType($array['file_type']);
            $kfile->setFileSize($array['file_size']);
            $kfile->setFileAddDate($array['file_add_date']);
            $kfile->setFileAddIp($array['file_add_ip']);
            $kfile->setFileDocumentRoot($array['document_root']);

            $outArr[] = $kfile;
        }

        return $outArr;
    }

    /**
     * Возращает список объектов KFile по ID файлов
     * @param string Строка вида '1,2,3....'
     */
    function deleteFile($idList) {

        $query = "select * from k_files where id in (" . DB::getInstance()->escapeStr($idList) . "); ";
//        echo "\r\n" . $query . "\r\n";

        $result = DB::query($query);
        $array = false;

        while ($array = mysql_fetch_array($result)) {

            $oldPathToFile = $array['document_root'] . $array['file_path'];
            $oldFileDir = substr($array['file_path'], 0, strrpos($array['file_path'], "/"));
            $newFileDir = str_replace("/uploads/", "/uploads/deleted/", $oldFileDir);
            $newFileDir = $newFileDir;
            mkdir($array['document_root'] . $newFileDir, 0777, true);

            $fileDeletedPath = str_replace($oldFileDir, $newFileDir, $oldPathToFile);

//            echo "\r\n newFileDir " . $newFileDir . "\r\n";
//            echo "\r\n oldPathToFile " . $oldPathToFile . "\r\n";
//            echo "\r\n fileDeletedPath " . $fileDeletedPath . "\r\n";

            rename($oldPathToFile, $fileDeletedPath);

            $query = "update  k_files "
                    . " set file_del_path = '" . $fileDeletedPath . "' "
                    . " , file_del_date = NOW() "
                    . " , file_del_ip = '" . APPLICATION::get_client_ip() . "' "
                    . " where id  =" . $array['id'] . "; ";
//            echo "\r\n" . $query . "\r\n";

            DB::query($query);
        }
    }

}

<?php

/**
 * Description of KLanding
 *
 * @author wwwdev
 */
class KLanding {

    public $cssFileKFile;
    public $bodyFileKFile;
    public $landing_id;
    public $landing_created_at;
    public $landing_name;
    public $landing_cssFileID;
    public $landing_bodyFileID;
    public $landing_deleted;

    public function getID() {
        return $this->landing_id;
    }

    public function getCreatedAt() {
        return $this->landing_created_at;
    }

    public function getName() {
        return $this->landing_name;
    }

    public function getCssFileID() {
        return $this->landing_cssFileID;
    }

    public function getBodyFileID() {
        return $this->landing_bodyFileID;
    }

    public function isDeleted() {
        if ($this->landing_deleted > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function setID($landing_id) {
        $this->landing_id = $landing_id;
    }

    public function setCreatedAt($landing_created_at) {
        $this->landing_created_at = $landing_created_at;
    }

    public function setName($landing_name) {
        $this->landing_name = $landing_name;
    }

    public function

    setCssFileID($landing_cssFileID) {
        $this->landing_cssFileID = $landing_cssFileID;
    }

    public function setBodyFileID($bodyFileID) {
        $this->landing_bodyFileID = $bodyFileID;
    }

    /**
     * 
     * @param int $landing_deleted - 0 - landing is NOT deleted, 1 - landing is deleted
     */
    public function setDeleted($landing_deleted) {
        $this->landing_deleted = $landing_deleted;
    }

    public function __construct($cssFile = false, $bodyFile = false) {

        if ($cssFile) {
            APPLICATION::includeModule("khartn:kfile");

            $cssFileKFile = new KFile();
            $cssFileKFile->setFileDocumentRoot($_SERVER["DOCUMENT_ROOT"]);
            $cssFileKFile->setFileAddIp(APPLICATION:: get_client_ip());
            $cssFileKFile->setFilePath(str_replace($_SERVER["DOCUMENT_ROOT"], "", $cssFile));
            $cssFileKFile->setFileType("css");
            $this->cssFileKFile = $cssFileKFile->saveToDB($cssFileKFile);
        }

        if ($bodyFile) {
            $bodyFileKFile = new KFile();
            $bodyFileKFile->setFileDocumentRoot($_SERVER["DOCUMENT_ROOT"]);
            $bodyFileKFile->setFileAddIp(APPLICATION:: get_client_ip());
            $bodyFileKFile->setFilePath(str_replace($_SERVER["DOCUMENT_ROOT"], "", $bodyFile));
            $bodyFileKFile->setFileType("htmlbody");

            $this->bodyFileKFile = $bodyFileKFile->saveToDB($bodyFileKFile);
        }
    }

    /**
     * 
     * @param type $ownerID
     * @return KLanding[]
     */
    public function getByOwnerID($ownerID) {
        APPLICATION::includeModule("khartn:kuser");

        $query = "select * from landings_landings "
                . "where ownerid=" . DB::getInstance()->escapeStr($ownerID) . " "
                . " and landings_deleted=0 "
                . "order by id desc; ";
//        echo "\r\n" . $query . "\r\n";

        $output = array();

        $result = DB::query($query);
        $array = false;

        while ($array = mysql_fetch_array($result)) {

            $landing = new KLanding();

            $landing->setID($array['id']);
            $landing->setCreatedAt($array['created_at']);
            $landing->setName($array['name']);
            $landing->setCssFileID($array['cssFileID']);
            $landing->setBodyFileID($array['bodyFileID']);
            $landing->setDeleted($array['landings_deleted']);

            $output[] = $landing;
        }

        return $output;
    }

    /**
     * 
     * @param type $landingID
     * @return KLanding
     */
    public function getByID($landingID) {
        APPLICATION::includeModule("khartn:kuser");

        $query = "select * from landings_landings "
                . "where ownerid=" . DB::getInstance()->escapeStr(Auth::$user->getUID()) . " "
                . "and id=" . DB::getInstance()->escapeStr($landingID) . " "
                . " and landings_deleted=0 "
                . "order by id desc; ";
//        echo "\r\n" . $query . "\r\n";

        $landing = false;

        $result = DB::query($query);
        $array = false;

        while ($array = mysql_fetch_array($result)) {

            $landing = new KLanding();

            $landing->setID($array['id']);
            $landing->setCreatedAt($array['created_at']);
            $landing->setName($array['name']);
            $landing->setCssFileID($array['cssFileID']);
            $landing->setBodyFileID($array['bodyFileID']);
            $landing->setDeleted($array['landings_deleted']);
        }

        return $landing;
    }

    public function save() {

        $landName = uniqid();

//        echo $landName;

        $query = "insert into landings_landings(created_from_ip, ownerid, name, cssFileID, bodyFileID) values ("
                . "'" . DB::getInstance()->escapeStr(APPLICATION::get_client_ip()) . "', "
                . "" . DB::getInstance()->escapeStr(Auth::$user->getUID()) . ", "
                . "'" . DB::getInstance()->escapeStr($landName) . "', "
                . "" . DB::getInstance()->escapeStr($this->cssFileKFile->getFileId()) . ", "
                . "" . DB::getInstance()->escapeStr($this->bodyFileKFile->getFileId()) . " "
                . ");";

//    echo "\r\n".$query."\r\n";

        DB::query($query);
        $landingID = DB::getLastInsertID();

        $query = "insert into landings_landings_files(file_id, user_id, landing_id) values ("
                . "" . DB::getInstance()->escapeStr($this->cssFileKFile->getFileId()) . ", "
                . "" . DB::getInstance()->escapeStr(Auth::$user->getUID()) . ", "
                . "" . $landingID . " "
                . ");";

//    echo "\r\n".$query."\r\n";

        DB::query($query);

        $query = "insert into landings_landings_files(file_id, user_id, landing_id) values ("
                . "" . DB::getInstance()->escapeStr($this->bodyFileKFile->getFileId()) . ", "
                . "" . DB::getInstance()->escapeStr(Auth::$user->getUID()) . ", "
                . "" . $landingID . " "
                . ");";

//    echo "\r\n".$query."\r\n";

        DB::query($query);

        return $landingID;
    }

    public function update() {


        $query = "select * from landings_landings "
                . "where ownerid=" . DB::getInstance()->escapeStr(Auth::$user->getUID()) . " "
                . "and name like '" . DB::getInstance()->escapeStr($this->landing_name) . "' "
                . " and landings_deleted=0 "
                . "order by id desc; ";
//        echo "\r\n" . $query . "\r\n";
        $result = DB::query($query);

        $landingID = false;
        while ($array = mysql_fetch_array($result)) {

            $landingID = $array['id'];
        }


        $query = "select file_id from landings_landings_files "
                . "where user_id=" . DB::getInstance()->escapeStr(Auth::$user->getUID()) . " "
                . "and landing_id=" . $landingID . " "
                . "and file_deleted_indicator=0"
                . ";";
        //        echo "\r\n" . $query . "\r\n";
        $result = DB::query($query);

        $filesToDelete = array();
        while ($array = mysql_fetch_array($result)) {
            $filesToDelete[] = $array['file_id'];
//            print_r($array);
        }

        $filesToDelete = implode(", ", $filesToDelete);
        $kfile = new KFile();
        $kfile->deleteFile($filesToDelete);

        $query = "update landings_landings_files "
                . "set file_deleted_indicator=1 , file_delete_date=NOW() "
                . "where user_id=" . DB::getInstance()->escapeStr(Auth::$user->getUID()) . " "
                . "and landing_id=" . $landingID . " "
                . "and file_deleted_indicator=0"
                . ";";

        DB::query($query);

        $query = "insert into landings_landings_files(file_id, user_id, landing_id) values ("
                . "" . DB::getInstance()->escapeStr($this->cssFileKFile->getFileId()) . ", "
                . "" . DB::getInstance()->escapeStr(Auth::$user->getUID()) . ", "
                . "" . $landingID . " "
                . ");";

//    echo "\r\n".$query."\r\n";

        DB::query($query);

        $query = "insert into landings_landings_files(file_id, user_id, landing_id) values ("
                . "" . DB::getInstance()->escapeStr($this->bodyFileKFile->getFileId()) . ", "
                . "" . DB::getInstance()->escapeStr(Auth::$user->getUID()) . ", "
                . "" . $landingID . " "
                . ");";

//    echo "\r\n".$query."\r\n";

        DB::query($query);

        $query = "update landings_landings "
                . "set cssFileID=" . DB::getInstance()->escapeStr($this->cssFileKFile->getFileId()) . ", "
                . "bodyFileID=" . DB::getInstance()->escapeStr($this->bodyFileKFile->getFileId()) . " "
                . "where ownerid =" . DB::getInstance()->escapeStr(Auth::$user->getUID()) . " "
                . "and id=" . $landingID
                . ";";
//    echo "\r\n".$query."\r\n";
        DB::query($query);

        $query = "insert into landings_landings_edit_history(landing_id	, ip) values ("
                . "" . $landingID . ", "
                . "'" . APPLICATION::get_client_ip() . "' "
                . ");";

//    echo "\r\n".$query."\r\n";

        DB::query($query);
    }

}

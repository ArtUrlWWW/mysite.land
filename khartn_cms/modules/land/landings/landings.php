<?php

/**
 * Description of KLandings
 *
 * @author wwwdev
 */
class KLandings {

    /**
     * Помечает landings файл как удалённый
     * @param int $uID - ID пользователя
     * @param int $fileId - ID файла
     * 
     */
    public static function deleteLandingsFile($fileId, $uID) {
        $query = "update landings_landings_files "
                . "set file_deleted_indicator=1 "
                . "where user_id=" . DB::getInstance()->escapeStr($uID) . " "
                . " and file_id=" . DB::getInstance()->escapeStr($fileId) . " "
                . " and file_deleted_indicator=0; ";
//        echo "\r\n" . $query . "\r\n";

        $ids = array();

        $result = DB::query($query);
        $array = false;

        while ($array = mysql_fetch_array($result)) {
            $ids[] = $array['file_id'];
        }

        return $ids;
    }

    /**
     * Проверяет, является ли пользователь владельцем файла
     * @param int $uID - ID пользователя
     * @param int $fileID - ID файла
     * 
     */
    public static function checkFileOwner($fileID, $uID) {
        $query = "select * from landings_landings_files where user_id=" . DB::getInstance()->escapeStr($uID) .
                " and file_id=" . DB::getInstance()->escapeStr($fileID) . "; ";
//        echo "\r\n" . $query . "\r\n";

        $ids = array();

        $result = DB::query($query);
        $array = false;

        while ($array = mysql_fetch_array($result)) {
            $ids[] = $array['file_id'];
        }

        if (count($ids) > 0) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Возвращает список IDшников файлов, принадлежащих пользователю
     * @param int $uID - ID пользователя, файлы которого необходиом найти
     * 
     */
    public static function getLandingsFilesIDsByUserID($uID) {
            $query = "select * from landings_landings_files "
                    . "where user_id=" . DB::getInstance()->escapeStr($uID) . " "
                    . " and file_deleted_indicator=0; ";
//        echo "\r\n" . $query . "\r\n";

        $ids = array();

        $result = DB::query($query);
        $array = false;

        while ($array = mysql_fetch_array($result)) {
            $ids[] = $array['file_id'];
        }

        return $ids;
    }

    /**
     * 
     * @param KFile $kfile
     * @return type
     */
    public static function insertNewLandingFileToDB($kfile, $landingID = false) {
        if (!$landingID) {
            $query = "insert into landings_landings_files(file_id, user_id) values ("
                    . "" . DB::getInstance()->escapeStr($kfile->getFileId()) . ", "
                    . "" . DB::getInstance()->escapeStr(Auth::$user->getUID()) . " "
                    . ");";

//    echo "\r\n".$query."\r\n";

            DB::query($query);
            return DB::getLastInsertID();
        } else {
            $query = "insert into landings_landings_files(file_id, user_id, landing_id) values ("
                    . "" . DB::getInstance()->escapeStr($kfile->getFileId()) . ", "
                    . "" . DB::getInstance()->escapeStr(Auth::$user->getUID()) . " "
                    . ");";

//    echo "\r\n".$query."\r\n";

            DB::query($query);
            return DB::getLastInsertID();
        }
    }

}

-- phpMyAdmin SQL Dump
-- version 3.5.8.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 07, 2014 at 11:57 AM
-- Server version: 5.5.31-log
-- PHP Version: 5.4.23

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `u1326_land`
--

-- --------------------------------------------------------

--
-- Table structure for table `k_files`
--

CREATE TABLE IF NOT EXISTS `k_files` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `document_root` varchar(777) NOT NULL,
  `file_name` varchar(77) NOT NULL,
  `file_path` varchar(777) NOT NULL,
  `file_type` varchar(77) NOT NULL,
  `file_size` float NOT NULL,
  `file_owner` int(11) NOT NULL,
  `file_add_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `file_add_ip` varchar(150) NOT NULL,
  `file_del_path` varchar(777) NOT NULL,
  `file_del_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `file_del_ip` varchar(150) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `file_name` (`file_name`,`file_type`,`file_size`,`file_add_date`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `k_logins`
--

CREATE TABLE IF NOT EXISTS `k_logins` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL,
  `auth_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `auth_ip` varchar(777) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `k_logins`
--

INSERT INTO `k_logins` (`id`, `uid`, `auth_date`, `auth_ip`) VALUES
(1, 1, '2014-02-07 07:16:55', '78.138.134.240');

-- --------------------------------------------------------

--
-- Table structure for table `k_users`
--

CREATE TABLE IF NOT EXISTS `k_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(777) NOT NULL,
  `passwd` varchar(777) NOT NULL,
  `phone` varchar(150) NOT NULL,
  `reg_ip` varchar(350) NOT NULL,
  `checkSMSCode` varchar(777) NOT NULL,
  `reg_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `surname` varchar(70) NOT NULL DEFAULT '',
  `name` varchar(70) NOT NULL DEFAULT '',
  `patronymic` varchar(70) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `username` (`username`(333)),
  KEY `phone` (`phone`),
  KEY `surname` (`surname`),
  KEY `name` (`name`),
  KEY `patronymic` (`patronymic`),
  KEY `patronymic_2` (`patronymic`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `k_users`
--

INSERT INTO `k_users` (`id`, `username`, `passwd`, `phone`, `reg_ip`, `checkSMSCode`, `reg_date`, `surname`, `name`, `patronymic`) VALUES
(1, 'forpdfsending@gmail.com', '*5E8870230DEAABEBF48AA4E359D409A92296295B', '79178708087', '78.138.134.240', '2569', '2014-01-09 02:20:42', '', '', ''),
(2, 'khartnjava@gmail.com', '*80120EA5314E420452D15417065E4BA93AB3780C', '435345345345345', '78.138.134.240', '12477', '2014-02-03 01:49:54', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `k_users_custom_fields`
--

CREATE TABLE IF NOT EXISTS `k_users_custom_fields` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL,
  `name` varchar(777) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `uid` (`uid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `k_users_custom_fields_values`
--

CREATE TABLE IF NOT EXISTS `k_users_custom_fields_values` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `field_id` int(11) NOT NULL,
  `value` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `field_id` (`field_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `landings_landings`
--

CREATE TABLE IF NOT EXISTS `landings_landings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_from_ip` varchar(150) NOT NULL,
  `ownerid` int(11) NOT NULL,
  `name` varchar(777) NOT NULL,
  `cssFileID` int(11) NOT NULL,
  `bodyFileID` int(11) NOT NULL,
  `landings_deleted` int(11) NOT NULL DEFAULT '0',
  UNIQUE KEY `id` (`id`),
  KEY `ownerid` (`ownerid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `landings_landings_files`
--

CREATE TABLE IF NOT EXISTS `landings_landings_files` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `file_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `add_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `file_deleted_indicator` int(11) NOT NULL DEFAULT '0',
  `landing_id` int(11) NOT NULL,
  `file_delete_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

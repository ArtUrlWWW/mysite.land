var maxZindex = 100000;
var stylesToRevert;

var toggleCodeAndBrowserBView = 0;

$(document).ready(function() {

    $("#backgroundColorOpacity").keyup(function(e) {
        
        var element = document.getElementById("landing").contentWindow.getCurrentSelectedElement();

        var backgroundColor = $(element).css('background-color');

        document.getElementById("landing").contentWindow.renderPre($(element));

//        console.log(backgroundColor);

        if (backgroundColor.indexOf("rgba") > -1) {

            var opacit = backgroundColor.replace("rgba", "").replace("(", "").replace(")", "")
            opacit = opacit.replace(" ", "");
            opacit = opacit.replace(" ", "");
            opacit = opacit.replace(" ", "");
            opacit = opacit.replace(" ", "");
            opacit = opacit.split(",");
//            $("#backgroundColorOpacity").val(opacit[3] * 100);

            var opNew = $("#backgroundColorOpacity").val();
            opNew = opNew.replace(" ", "");
            opNew = opNew.replace("%", "");
            opNew = parseFloat(opNew) / 100;

            var bgColorWithNewOpacity = "rgba(" + opacit[0] + ", " + opacit[1] + ", " + opacit[2] + ", " + opNew + ")";
//            console.log(bgColorWithNewOpacity);

            $(element).css('background-color', bgColorWithNewOpacity);

        } else if (backgroundColor.indexOf("rgb") > -1) {

            var opacit = backgroundColor.replace("rgb", "").replace("(", "").replace(")", "")
            opacit = opacit.replace(" ", "");
            opacit = opacit.replace(" ", "");
            opacit = opacit.replace(" ", "");
            opacit = opacit.replace(" ", "");
            opacit = opacit.split(",");
//            $("#backgroundColorOpacity").val(opacit[3] * 100);

            var opNew = $("#backgroundColorOpacity").val();
            opNew = opNew.replace(" ", "");
            opNew = opNew.replace("%", "");
            opNew = parseFloat(opNew) / 100;

            var bgColorWithNewOpacity = "rgba(" + opacit[0] + ", " + opacit[1] + ", " + opacit[2] + ", " + opNew + ")";
//            console.log(bgColorWithNewOpacity);
            $(element).css('background-color', bgColorWithNewOpacity);
        } else if (backgroundColor.indexOf("#") > -1) {

            backgroundColor = hexToRgb(backgroundColor);

            var opNew = $("#backgroundColorOpacity").val();
            opNew = opNew.replace(" ", "");
            opNew = opNew.replace("%", "");
            opNew = parseFloat(opNew) / 100;

            var bgColorWithNewOpacity = "rgba(" + backgroundColor.r + ", " + backgroundColor.g + ", " + backgroundColor.b + ", " + opNew + ")";
//            console.log(bgColorWithNewOpacity);
            $(element).css('background-color', bgColorWithNewOpacity);

        }

        document.getElementById("landing").contentWindow.renderPost($(element), "changeBgColor");

    });

    $(".icon-code").click(function(e) {

        if (toggleCodeAndBrowserBView < 1) {

            document.getElementById("landing").contentWindow.cleanDragAndResize();
            var a = $("iframe#landing");
            var pageContent = $(a).contents();
            var bodyHtml = $("body", pageContent);
            $(a).hide();
            var textArea = $("<textarea id='codeOfPage'>");
            $(textArea).text($(bodyHtml).html());
            $(textArea).appendTo($(".content-wrapper"));
            $(textArea).css("width", "90%");
            $(textArea).css("height", "100%");
            $(textArea).css("right", "0");
            $(textArea).css("top", "0");
            $(textArea).css("position", "absolute");
            toggleCodeAndBrowserBView = 1;
        } else {
            toggleCodeAndBrowserBView = 0;

            document.getElementById("landing").contentWindow.cleanDragAndResize();
            var a = $("iframe#landing");
            var pageContent = $(a).contents();
            var bodyHtml = $("body", pageContent);
            $(bodyHtml).html($("#codeOfPage").val());
            $("#codeOfPage").remove();
            $(a).show();
        }

    });

    window.openRightMenuSettingsSection = function() {
        openRightMenuSection("#settingsListHolder");

        var backgroundColor = $(document.getElementById("landing").contentWindow.getCurrentSelectedElement()).css('background-color');

        if (backgroundColor.indexOf("rgba") > -1) {
            if (backgroundColor.indexOf("rgba(0, 0, 0, 0)") > -1) {
                backgroundColor = "rgb(255,255,255)";
                backgroundColor = "#" + getHexRGBColor(backgroundColor);

                $('.backgroundColorPickerButton').css('background-color', backgroundColor);

            } else {
                var opacit = backgroundColor.replace("rgba", "").replace("(", "").replace(")", "")
                opacit = opacit.replace(" ", "");
                opacit = opacit.replace(" ", "");
                opacit = opacit.replace(" ", "");
                opacit = opacit.replace(" ", "");
                opacit = opacit.split(",");
                $("#backgroundColorOpacity").val(opacit[3] * 100);

                $('.backgroundColorPickerButton').css('background-color', backgroundColor);

                backgroundColor = "rgb (";
                backgroundColor = backgroundColor + opacit[0] + ", ";
                backgroundColor = backgroundColor + opacit[1] + ", ";
                backgroundColor = backgroundColor + opacit[2] + ") ";
                backgroundColor = "#" + getHexRGBColor(backgroundColor);
            }

        } else if (backgroundColor.indexOf("rgb") > -1) {
            backgroundColor = "#" + getHexRGBColor(backgroundColor);

            $('.backgroundColorPickerButton').css('background-color', backgroundColor);

        }

        // background-color: rgb(1, 174, 240);
        // background-color: rgba(1, 174, 240, 0.5);
//        console.log("back " + backgroundColor);

        $('.backgroundColorPickerButton').ColorPicker({
            color: backgroundColor,
            onBeforeShow: function() {
                $(this).ColorPickerSetColor(backgroundColor);
                var element = document.getElementById("landing").contentWindow.getCurrentSelectedElement();
                document.getElementById("landing").contentWindow.renderPre($(element));
            },
            onShow: function(colpkr) {
                $(colpkr).fadeIn(500);
                return false;
            },
            onHide: function(colpkr) {
                $(colpkr).fadeOut(500);
                return false;
            },
            onChange: function(hsb, hex, rgb) {
//            console.log(rgb);
                var opacity = $("#backgroundColorOpacity").val();
                opacity = opacity.replace("%", "");
                opacity = parseFloat(opacity);
//            console.log(opacity);
                opacity = opacity / 100;
//            console.log(opacity);
                $('.backgroundColorPickerButton').css('background-color', 'rgba(' + rgb.r + ", " + rgb.g + ", " + rgb.b + ", " + opacity + ")");
                var element = document.getElementById("landing").contentWindow.getCurrentSelectedElement();
                $(element).css('background-color', 'rgba(' + rgb.r + ", " + rgb.g + ", " + rgb.b + ", " + opacity + ")");
                document.getElementById("landing").contentWindow.renderPost($(element), "changeBgColor");
            }
        });

        var textColor = $(document.getElementById("landing").contentWindow.getCurrentSelectedElement()).css('color');

        if (textColor === undefined) {
            textColor = "rgb(0,0,0)";
        }

        textColor = "#" + getHexRGBColor(textColor);
//        console.log("textColor "+textColor);
        $('.textColorPickerButton').css('background-color', textColor);
        $('.textColorPickerButton').ColorPicker({
            color: textColor,
            onBeforeShow: function() {
                var element = document.getElementById("landing").contentWindow.getCurrentSelectedElement();
                document.getElementById("landing").contentWindow.renderPre($(element));
            },
            onShow: function(colpkr) {
                $(colpkr).fadeIn(500);
                return false;
            },
            onHide: function(colpkr) {
                $(colpkr).fadeOut(500);
                return false;
            },
            onChange: function(hsb, hex, rgb) {
//            console.log(hex);
                $('.textColorPickerButton').css('backgroundColor', '#' + hex);
                var element = document.getElementById("landing").contentWindow.getCurrentSelectedElement();
                $(element).css('color', '#' + hex);
                $(".editableText", element).css('color', '#' + hex);
                document.getElementById("landing").contentWindow.renderPost($(element), "changeTextColor");
            }
        });

    };

    $(".body-wrapper .left-side-wrapper-main a").on({
        mouseenter: function() {

            var prnt1 = $("#toogleSettingsList").parent();
            $(".popup", prnt1).css("display", "block");
            $(".popup", prnt1).css("visibility", "visible");
            $(".popup", prnt1).css("visibility", "hidden");
            var prnt = $(this).parent();
            $(".popup", prnt).css("display", "block");
            $(".popup", prnt).css("visibility", "visible");
        },
        mouseleave: function() {
            var prnt = $(this).parent();
            $(".popup", prnt).css("visibility", "hidden");
        }
    });
    
    $(".undoButton").click(function(e) {
        document.getElementById("landing").contentWindow.undo();
    });
    
    $("#savePageButton").click(function(e) {
        
        document.getElementById("landing").contentWindow.closeAllFileUploadForms();

        var usedFonts = document.getElementById("landing").contentWindow.getUsedFonts();
        usedFonts = getUnique(usedFonts);
        document.getElementById("landing").contentWindow.cleanDragAndResize();
        var a = $("iframe#landing");
        var pageContent = $(a).contents();
//        //Clean text holders
//        var textHolders = $(".textHolder", pageContent);
//        var textHoldersLength = $(".textHolder", pageContent).length;
//
//        for (var x = 0; x < textHoldersLength; x++) {
//            var textHolderElement = textHolders[x];
//            var textHolderElementsEditableTextCount = $(".editableText", textHolderElement).length;
//            if (textHolderElementsEditableTextCount > 0) {
//                var textHolderElementsEditableText = $(".editableText", textHolderElement);
//                $(textHolderElement).html($(textHolderElementsEditableText).html());
//            }
//        }

        // Находим все элементы, имеющие data-styleid для сохранения страх стилей
        var elementsWithCustomStyles = $("[data-styleid]", pageContent);
        var allStyles = [];
        for (var x = 0; x < elementsWithCustomStyles.length; x++) {
            var elementCS = elementsWithCustomStyles[x];
            var styleID = $(elementCS).attr("data-styleid");
            if (jQuery.inArray(styleID, allStyles) < 0) {
                allStyles.push(styleID);
            }
        }

        var fontsLinks = $("link.fontsList", pageContent);
        for (var x = 0; x < fontsLinks.length; x++) {
            var fl = fontsLinks[x];
            var fontFamName = $(fl).attr("data-name");
            if (jQuery.inArray(fontFamName, usedFonts) < 0) {
                $(fl).remove();
            }
        }

        var styles = $("style", pageContent);
        var stylesCount = $(styles).length;
        var stylesMainContent = "";
        for (var x = 0; x < stylesCount; x++) {
            stylesMainContent += $(styles[x]).text() + "\r\n";
        }

        stylesToRevert = $("style", pageContent).clone();
        $("style", pageContent).remove();
        var cssName = generateUid("");
        var linkHref = $("<link>");
        $(linkHref).attr("href", "/uploads/landings-css/" + cssName + ".css");
        $(linkHref).attr("rel", "stylesheet");
        var header = $("head", pageContent);
        var lastStyleFile = $("link[href*='/uploads/landings-css/']", header).last();
        lastStyleFile = $(lastStyleFile).attr("href");
        $.post("/proxy/oldStyles.php",
                {
                    lastStyleFile: lastStyleFile,
                    allStyles: allStyles,
                },
                function(data) {
//                    console.log(stylesMainContent);
//                    console.log("+++++++++++++++++++++++++++");
//                    console.log(data);
//                    console.log("+++++++++++++++++++++++++++");
                    stylesMainContent += data;
//                    console.log(stylesMainContent);
//                    console.log("+++++++++++++++++++++++++++");

                    $("link[href*='/uploads/landings-css/']", header).remove();
                    $(linkHref).appendTo($(header));
                    a = $("iframe#landing");
                    pageContent = $(a).contents();
                    var htmll = $("html", pageContent);
                    var htmlCont = $(htmll).html();
                    htmlCont = "<!DOCTYPE html><html>" + htmlCont + "</html>";
                    $.post("/khartn_cms/components/khartn/landings.create.new.editor/save_page.php",
                            {
                                stylesMainContent: stylesMainContent,
                                cssName: cssName,
                                cssNameFull: "/uploads/landings-css/" + cssName + ".css",
                                pageContent: htmlCont,
                                landingNameGlobal: landingNameGlobal,
                                allStyles: allStyles,
                            },
                            function(data1) {
                                //  console.log(data1);
                                var a = $("iframe#landing");
                                var pageContent = $(a).contents();
                                var header = $("head", pageContent);
                                $(stylesToRevert).appendTo($(header));
                                window.location.href = "/";
                            }, "text");
                }, "text");
        e.stopPropagation();
        e.preventDefault();
        return false;
    });
    window.openRightMenuSection = function(section) {

        var sectionLocal = $(section);
        $(sectionLocal).siblings().not(".left-side-wrapper").hide("slide", {direction: "left"}, 100, function() {
            $(".body-wrapper .left-side-wrapper-main").css("width", "61px");
        });
        if ($(sectionLocal).is(":visible")) {
            $(sectionLocal).hide("slide", {direction: "left"}, 100, function() {
                $(".body-wrapper .left-side-wrapper-main").css("width", "122px");
            });
        } else {
            $(sectionLocal).show("slide", {direction: "left"}, 300, function() {
                $(".body-wrapper .left-side-wrapper-main").css("width", "122px");
            });
        }
    };
    $("#toogleElementsList").click(function(e) {
        openRightMenuSection("#elementsListHolder");
        return false;
    });
    $("#toogleSettingsList").click(function() {

        openRightMenuSection("#settingsListHolder");
        return false;
    });
    $("#landing").load(function() {

        var $this = $(this);
        var contents = $this.contents();
        var header = $("head", contents);
        var fontStyle2 = $("<link>");
        $(fontStyle2).addClass("fontOpen-sans");
        $(fontStyle2).addClass("fontsList");
        $(fontStyle2).attr("data-name", "Open Sans");
        $(fontStyle2).attr("data-type", "sans-serif");
        $(fontStyle2).attr("href", "http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,400,300,600&subset=latin,cyrillic-ext,latin-ext,cyrillic");
        $(fontStyle2).attr("rel", "stylesheet");
        $(fontStyle2).attr("type", "text/css");
        $(fontStyle2).appendTo($(header));
        var fontStyleOne = $("<link>");
        $(fontStyleOne).addClass("fontEb-garamond");
        $(fontStyleOne).addClass("fontsList");
        $(fontStyleOne).attr("data-name", "EB Garamond");
        $(fontStyleOne).attr("data-type", "serif");
        $(fontStyleOne).attr("href", "http://fonts.googleapis.com/css?family=EB+Garamond&subset=cyrillic,latin,cyrillic-ext,latin-ext");
        $(fontStyleOne).attr("rel", "stylesheet");
        $(fontStyleOne).attr("type", "text/css");
        $(fontStyleOne).appendTo($(header));
        var fontStyle3 = $("<link>");
        $(fontStyle3).addClass("fontPT-Sans-Narrow");
        $(fontStyle3).addClass("fontsList");
        $(fontStyle3).attr("data-type", "sans-serif");
        $(fontStyle3).attr("data-name", "PT Sans Narrow");
        $(fontStyle3).attr("href", "http://fonts.googleapis.com/css?family=PT+Sans+Narrow:400,700&subset=latin,cyrillic-ext,latin-ext,cyrillic");
        $(fontStyle3).attr("rel", "stylesheet");
        $(fontStyle3).attr("type", "text/css");
        $(fontStyle3).appendTo($(header));
        var fontStyle4 = $("<link>");
        $(fontStyle4).addClass("fontUbuntu");
        $(fontStyle4).addClass("fontsList");
        $(fontStyle4).attr("data-type", "sans-serif");
        $(fontStyle4).attr("data-name", "Ubuntu");
        $(fontStyle4).attr("href", "http://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700,300italic,400italic,500italic,700italic&subset=latin,cyrillic-ext,latin-ext,cyrillic");
        $(fontStyle4).attr("rel", "stylesheet");
        $(fontStyle4).attr("type", "text/css");
        $(fontStyle4).appendTo($(header));
        var fontStyle5 = $("<link>");
        $(fontStyle5).addClass("fontPT-Serif");
        $(fontStyle5).addClass("fontsList");
        $(fontStyle5).attr("data-type", "serif");
        $(fontStyle5).attr("data-name", "PT Serif");
        $(fontStyle5).attr("href", "http://fonts.googleapis.com/css?family=PT+Serif:400,700,400italic,700italic&subset=latin,cyrillic,latin-ext,cyrillic-ext");
        $(fontStyle5).attr("rel", "stylesheet");
        $(fontStyle5).attr("type", "text/css");
        $(fontStyle5).appendTo($(header));
        $(contents.find('div')).addClass("already-dropped");


//        contents.find('body').resizable({        });
//        contents.find('body').resizable({handles: "n, e, s, w, ne, se, sw, nw",
//            stop: function(event, ui) {
//
//            }
//        });


        contents.find('body').droppable({
            iframeFix: true,
            drop: function(event, ui) {

                if (!ui.helper.hasClass("already-dropped")) {

                    var draggableElement = ui.draggable;
                    /*
                     * Добавление текстового поля
                     */
                    if ($(draggableElement).hasClass("add-text-action")) {

                        var xPos = event.pageX;
                        var yPos = event.pageY;
                        yPos = yPos - 54;
                        var someDiv = $("<div>");
                        $(someDiv).css("position", "absolute");
                        $(someDiv).css("left", xPos);
                        $(someDiv).css("top", yPos);
                        $(someDiv).css("width", 100);
                        $(someDiv).css("height", 30);
                        $(someDiv).css("border", "solid 1px rgb(1, 174, 240)");
                        $(someDiv).css("z-index", maxZindex);
                        $(someDiv).addClass("textHolder");
                        $(someDiv).addClass("already-dropped");
                        $(someDiv).attr("contenteditable", "true");
                        $(someDiv).html("Some text...");
                        maxZindex++;
                        someDiv.appendTo($(this));
                    }

                    /*
                     * End of Добавление текстового поля
                     */

                    /*
                     * Добавление поля с рисунком
                     */
                    if ($(draggableElement).hasClass("add-image-action")) {

                        var xPos = event.pageX;
                        var yPos = event.pageY;
                        yPos = yPos - 54;
                        var someDiv = $("<div>");
                        $(someDiv).css("position", "absolute");
                        $(someDiv).css("left", xPos);
                        $(someDiv).css("top", yPos);
                        $(someDiv).css("width", 200);
                        $(someDiv).css("height", 200);
                        $(someDiv).css("border", "solid 1px rgb(1, 174, 240)");
                        $(someDiv).css("z-index", maxZindex);
                        $(someDiv).addClass("imageHolder");
                        $(someDiv).addClass("already-dropped");
                        $(someDiv).attr("contenteditable", "true");
                        $(someDiv).html("<div class='clickOnMe systemDIV' style='height: 100%;'>Click on me...</div>");
                        maxZindex++;
                        someDiv.appendTo($(this));
                    }

                    /*
                     * End of Добавление поля с рисунком
                     */
                }
            }
        });
//        resize();
        document.getElementById("landing").contentWindow.resize();
    });
    onResizeHandler();
    $(window).resize(function() {
        onResizeHandler();
    });
    $(".add-text-action").draggable({
        iframeFix: true, helper: "clone",
        cursor: "move", scroll: true, scrollSensitivity: 50, scrollSpeed: 50,
//        stop: function(event, ui) {
//
//            var xPos = event.pageX - ($(this).height() / 2);
//            var yPos = event.pageY - ($(this).height() / 2);
//
//            console.log("!!! xPos " + xPos + " ");
//            console.log("!!! yPos " + yPos + " ");
//
//
//        }
    });
    $(".add-image-action").draggable({
        iframeFix: true, helper: "clone",
        cursor: "move", scroll: true, scrollSensitivity: 50, scrollSpeed: 50,
    });
    $(".clean-drag-and-resize").click(function() {
        document.getElementById("landing").contentWindow.cleanDragAndResize();
        return false;
    });
//End of document ready
});
function getUnique(arrayVar) {
    var u = {}, a = [];
    for (var i = 0, l = arrayVar.length; i < l; ++i) {
        if (u.hasOwnProperty(arrayVar[i])) {
            continue;
        }
        a.push(arrayVar[i]);
        u[arrayVar[i]] = 1;
    }
    return a;
}

function onResizeHandler() {
    $(".left-side-wrapper-main").css("height", $("body").height() - 75);
    $(".left-side-slide-wrapper").css("height", $("body").height() - 63);
    $("#landing").css("height", $("body").height() - 55);
}


/*
 * getStyleObject Plugin for jQuery JavaScript Library
 * From: http://upshots.org/?p=112
 *
 * Copyright: Unknown, see source link
 * Plugin version by Dakota Schneider (http://hackthetruth.org)
 * http://stackoverflow.com/questions/1004475/jquery-css-plugin-that-returns-computed-style-of-element-to-pseudo-clone-that-el
 * 
 * Usage: 
 var style = $("#original").getStyleObject(); // copy all computed CSS properties
 $("#original").clone() // clone the object
 .parent() // select it's parent
 .appendTo() // append the cloned object to the parent, after the original
 // (though this could really be anywhere and ought to be somewhere
 // else to show that the styles aren't just inherited again
 .css(style); // apply cloned styles
 */

(function($) {
    $.fn.getStyleObject = function() {
        var dom = this.get(0);
        var style;
        var returns = {};
        if (window.getComputedStyle) {
            var camelize = function(a, b) {
                return b.toUpperCase();
            }
            style = window.getComputedStyle(dom, null);
            for (var i = 0; i < style.length; i++) {
                var prop = style[i];
                var camel = prop.replace(/\-([a-z])/g, camelize);
                var val = style.getPropertyValue(prop);
                returns[camel] = val;
            }
            return returns;
        }
        if (dom.currentStyle) {
            style = dom.currentStyle;
            for (var prop in style) {
                returns[prop] = style[prop];
            }
            return returns;
        }
        return this.css();
    }
})(jQuery);
var generateUid = function(separator) {
    /// <summary>
    ///    Creates a unique id for identification purposes.
    /// </summary>
    /// <param name="separator" type="String" optional="true">
    /// The optional separator for grouping the generated segmants: default "-".    
    /// </param>

    var delim = separator || "";
//                    var delim = separator || "-";

    function S4() {
        return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
    }

    return (S4() + S4() + delim + S4() + delim + S4() + delim + S4() + delim + S4() + S4() + S4());
};

function getHexRGBColor(color)
{
    color = color.replace(/\s/g, "");
    var aRGB = color.match(/^rgb\((\d{1,3}[%]?),(\d{1,3}[%]?),(\d{1,3}[%]?)\)$/i);

    if (aRGB)
    {
        color = '';
        for (var i = 1; i <= 3; i++)
            color += Math.round((aRGB[i][aRGB[i].length - 1] == "%" ? 2.55 : 1) * parseInt(aRGB[i])).toString(16).replace(/^(.)$/, '0$1');
    }
    else
        color = color.replace(/^#?([\da-f])([\da-f])([\da-f])$/i, '$1$1$2$2$3$3');

    return color;
}

function hexToRgb(hex) {
    // Expand shorthand form (e.g. "03F") to full form (e.g. "0033FF")
    var shorthandRegex = /^#?([a-f\d])([a-f\d])([a-f\d])$/i;
    hex = hex.replace(shorthandRegex, function(m, r, g, b) {
        return r + r + g + g + b + b;
    });

    var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
    return result ? {
        r: parseInt(result[1], 16),
        g: parseInt(result[2], 16),
        b: parseInt(result[3], 16)
    } : null;
}
var divForFile;
var usedFonts = [];
var undoArray = [];
var undoArrayPre = [];
var undoCounter = -1;
var drg = "";

var lastSelectedElement;
var lastSelectedElementsZindex;


/*
 * 
 */
var undoElementsArrayPre = [];
var undoElementsStylesArrayPre = [];
var undoOperations = [];



function getUsedFonts() {
    uf = [];
    var allElements = $("*");
    for (var x = 0; x < allElements.length; x++) {
        var fontFamily = $(allElements[x]).css("font-family");
        var fam = fontFamily.substr(fontFamily.indexOf("'") + 1);
        fam = fam.substr(0, fam.indexOf("'"));
        uf.push(fam);
    }

    return uf;
}

function resize() {

    $(document).ready(function() {
        $(document).bind("contextmenu", function(e) {
            e.preventDefault();
        });
    });

//    $(document).on("mousedown", "img", function(e) {
//        if (e.button == 2) {
//            alert('Right mouse button!');
//
//            return false;
//        }
//    });

    $(document).on("click", "img:not(.systemDIV)", function(e) {

        cleanDragAndResize();

        var div = $("<div>");
        $(div).addClass("imageHolder");
        $(div).addClass("already-dropped");
        $(div).css("display", "inline-block");
        $(div).css("position", "absolute");


        $(div).css("left", $(this).css("left"));
        $(div).css("top", $(this).css("top"));
        $(div).css("height", $(this).css("height"));
        $(div).css("width", $(this).css("width"));

        $(this).css("position", "relative");
        $(this).css("left", "0px");
        $(this).css("top", "0px");

        if (!$(this).parent().hasClass("imageHolder")) {
            $(this).wrap($(div));
        }

        $(this).parent().css("border", "solid 1px rgb(1, 174, 240)");

        lastSelectedElement = $(this).parent('.imageHolder');
        lastSelectedElementsZindex = $(lastSelectedElement).css("z-index");
        if (lastSelectedElementsZindex == "auto") {
            lastSelectedElementsZindex = $(this).css("z-index");
        }
        $(lastSelectedElement).css("z-index", "777777");

        renderPre($(this).parent());

        $(this).resizable({
            resize: function(event, ui) {
                var element = $(ui.element);

                $(element).parent().css("height", $(element).css("height"));
                $(element).parent().css("width", $(element).css("width"));
            },
            stop: function(event, ui) {
                var element = $(ui.element);
                renderElement(element);
                renderPost($(element).parent(), "resizeImg");

            }
        });

        $('.imageHolder').draggable({
            appendTo: 'body',
            start: function(event, ui) {
                isDraggingMedia = true;

            },
            stop: function(event, ui) {
                isDraggingMedia = false;
                var element = $(ui.helper);
                var img = $("img", element);
                var uiWrapper = $(".ui-wrapper", element);

                $(element).css("position", "absolute");
                $(element).css("display", "inline-block");

                $(img).css("left", "0");
                $(img).css("top", "0");

                $(element).css("border", "solid 1px rgb(1, 174, 240)");

                renderElement(element);
                renderPost($(element), "dragImg");
            },
            create: function(event, ui) {

                var divControls = $("<div>");
                $(divControls).css("height", "20px");
                $(divControls).css("font-size", "18px");
                $(divControls).css("position", "absolute");
                $(divControls).css("padding", "5px 5px 7px 7px");
                $(divControls).css("min-width", "170px");
                $(divControls).css("background-color", "rgb(1, 174, 240);");
                $(divControls).addClass("divControls");
                $(divControls).addClass("systemDIV");

                var i = $("<i>");
                $(i).addClass("icon-move");
                $(i).css("float", "left");
                $(i).css("cursor", "move");
                $(i).css("color", "rgb(255, 255, 255)");
                $(i).appendTo($(divControls));

                var divText;
                divText = $("<div>");
                $(divText).css("margin", "0 3px 0 3px");
                $(divText).html("IMAGE");
                $(divText).css("float", "left");
                $(divText).appendTo($(divControls));


                i = $("<i>");
                $(i).addClass("icon-pencil");
                $(i).css("float", "left");
                $(i).css("cursor", "pointer");
                $(i).css("margin-left", "7px");
                $(i).css("color", "rgb(255, 255, 255)");
                $(i).appendTo($(divControls));

                i = $("<i>");
                $(i).addClass("icon-trash");
                $(i).css("float", "right");
                $(i).css("cursor", "pointer");
                $(i).css("margin-left", "15px");
                $(i).css("color", "rgb(255, 255, 255)");
                $(i).appendTo($(divControls));

                var uiWrapper = $(".ui-wrapper", $(this));

                var divControlsTop = $(divControls).clone();
                $(divControlsTop).css("top", "-33px");
                $(divControlsTop).appendTo(uiWrapper);

                var divControlsBottom = $(divControls).clone();
                $(divControlsBottom).css("bottom", "-33px");

                $(divControlsBottom).appendTo(uiWrapper);
            }
        });

        $(this).parent(".ui-wrapper").css("overflow", "visible");

        e.stopPropagation();
        e.preventDefault();
    });


    $(document).on("click", "div:not(.systemDIV)", function(e) {

        if (!$(this).hasClass("divControls")) {

            window.parent.openRightMenuSection("#settingsListHolder");

            cleanDragAndResize();

            renderPre($(this));

            $("a").attr("contenteditable", "true");



            e.stopPropagation();

            $(this).css("border", "solid 1px rgb(1, 174, 240)");

            if ($(this).hasClass("imageHolder")) {
                $(this).draggable({
                    cursor: "move", scroll: true, scrollSensitivity: 50, scrollSpeed: 50,
                    create: function(event, ui) {

                        var divControls = $("<div>");
                        $(divControls).css("height", "20px");
                        $(divControls).css("font-size", "18px");
                        $(divControls).css("position", "absolute");
                        $(divControls).css("padding", "5px 5px 7px 7px");
                        $(divControls).css("min-width", "156px");
                        $(divControls).css("background-color", "rgb(1, 174, 240);");
                        $(divControls).addClass("divControls");
                        $(divControls).addClass("systemDIV");
                        var i = $("<i>");
                        $(i).addClass("icon-move");
                        $(i).css("float", "left");
                        $(i).css("cursor", "move");
                        $(i).css("color", "rgb(255, 255, 255)");
                        $(i).appendTo($(divControls));

                        var divText;

                        divText = $("<div>");
                        $(divText).css("margin", "0 3px 0 3px");
                        $(divText).html("IMAGE");
                        $(divText).css("float", "left");
                        $(divText).appendTo($(divControls));

                        i = $("<i>");
                        $(i).addClass("icon-pencil");
                        $(i).css("float", "left");
                        $(i).css("cursor", "pointer");
                        $(i).css("margin-left", "7px");
                        $(i).css("color", "rgb(255, 255, 255)");
                        $(i).appendTo($(divControls));

                        i = $("<i>");
                        $(i).addClass("icon-trash");
                        $(i).css("float", "right");
                        $(i).css("cursor", "pointer");
                        $(i).css("margin-left", "15px");
                        $(i).css("color", "rgb(255, 255, 255)");
                        $(i).appendTo($(divControls));

                        var divControlsTop = $(divControls).clone();
                        $(divControlsTop).css("top", "-33px");
                        $(divControlsTop).appendTo($(this));

                        var divControlsBottom = $(divControls).clone();
                        $(divControlsBottom).css("bottom", "-33px");
                        $(divControlsBottom).appendTo($(this));

                    },
                    stop: function(event, ui) {
                        var element = $(ui.helper);
                        renderElement(element);
                        renderPost($(element), "dragText"); // !!!
                    }
                });
//                    .bind('click', function() {
//                        $(this).focus();
//                    });
            } else {

                if (!$(this).hasClass("textHolder")) {
                    $(this).addClass("textHolder");
                }

                if ($(this).children(".handler").length < 1) {

                    var innerHTML = $(this).html();
                    var handlerDiv = $("<div class='handler systemDIV'>");
                    $(this).html("");
                    $(handlerDiv).appendTo($(this));

                    innerHTML = "<div class='editableText'>" + innerHTML + "</div>";
                    $(innerHTML).appendTo($(this));


                }

                $(this).unbind("click");

                $(this).draggable({
                    cursor: "move", scroll: true, scrollSensitivity: 50, scrollSpeed: 50,
//                    handle: $('.text', $(this)),
                    create: function(event, ui) {

                        var divControls = $("<div>");
                        $(divControls).css("height", "20px");
                        $(divControls).css("font-size", "18px");
                        $(divControls).css("position", "absolute");
                        $(divControls).css("padding", "5px 5px 7px 7px");
                        $(divControls).css("min-width", "156px");
                        $(divControls).css("background-color", "rgb(1, 174, 240);");
                        $(divControls).addClass("divControls");
                        $(divControls).addClass("systemDIV");
                        var i = $("<i>");
                        $(i).addClass("icon-move");
                        $(i).css("float", "left");
                        $(i).css("cursor", "move");
                        $(i).css("color", "rgb(255, 255, 255)");
                        $(i).appendTo($(divControls));

                        var divText;


//                        $(this).addClass("textHolder");

                        divText = $("<div>");
                        $(divText).css("margin", "0 3px 0 3px");
                        $(divText).html("TEXT");
                        $(divText).css("float", "left");
                        $(divText).appendTo($(divControls));

                        var fontControls = $("<div>");
                        $(fontControls).css("height", "auto");
                        $(fontControls).css("float", "right");
                        $(fontControls).css("font-size", "15px");
                        $(fontControls).css("position", "absolute");
                        $(fontControls).css("padding", "10px 10px 10px 10px");
                        $(fontControls).css("width", "390px");
                        $(fontControls).css("background-color", "rgb(50, 50, 50);");
                        $(fontControls).css("color", "rgb(255, 255, 255);");
                        $(fontControls).addClass("fontControls");
                        $(fontControls).addClass("systemDIV");
                        $(fontControls).attr("contenteditable", "false");

                        var boldButton = $("<div>");
                        var boldI = $("<i>");
                        $(boldI).addClass("icon-bold");
                        $(boldI).appendTo($(boldButton));
                        $(boldButton).css("padding", "7px 7px 7px 7px");
                        $(boldButton).css("border", "1px solid rgb(255,255,255)");
                        $(boldButton).css("width", "23px");
                        $(boldButton).css("-webkit-border-radius", "5px");
                        $(boldButton).css("-moz-border-radius", "5px");
                        $(boldButton).css("border-radius", "5px");
                        $(boldButton).css("float", "left");
                        $(boldButton).css("cursor", "pointer");
                        $(boldButton).addClass("boldButton");
                        $(boldButton).addClass("systemDIV");
                        $(boldButton).appendTo($(fontControls));

                        var italicButton = $("<div>");
                        var italicI = $("<i>");
                        $(italicI).addClass("icon-italic");
                        $(italicI).appendTo($(italicButton));
                        $(italicButton).css("padding", "7px 7px 7px 7px");
                        $(italicButton).css("border", "1px solid rgb(255,255,255)");
                        $(italicButton).css("width", "23px");
                        $(italicButton).css("-webkit-border-radius", "5px");
                        $(italicButton).css("-moz-border-radius", "5px");
                        $(italicButton).css("border-radius", "5px");
                        $(italicButton).css("float", "left");
                        $(italicButton).css("margin-left", "10px");
                        $(italicButton).css("cursor", "pointer");

                        $(italicButton).addClass("italicButton");
                        $(italicButton).addClass("systemDIV");
                        $(italicButton).appendTo($(fontControls));

                        var strikeButton = $("<div>");
                        var strikeI = $("<i>");
                        $(strikeI).addClass("icon-strike");
                        $(strikeI).appendTo($(strikeButton));
                        $(strikeButton).css("padding", "7px 7px 7px 7px");
                        $(strikeButton).css("border", "1px solid rgb(255,255,255)");
                        $(strikeButton).css("width", "23px");
                        $(strikeButton).css("-webkit-border-radius", "5px");
                        $(strikeButton).css("-moz-border-radius", "5px");
                        $(strikeButton).css("border-radius", "5px");
                        $(strikeButton).css("float", "left");
                        $(strikeButton).css("margin-left", "10px");
                        $(strikeButton).css("cursor", "pointer");

                        $(strikeButton).addClass("strikeButton");
                        $(strikeButton).addClass("systemDIV");
                        $(strikeButton).appendTo($(fontControls));

                        var underlineButton = $("<div>");
                        var underlineI = $("<i>");
                        $(underlineI).addClass("icon-underline");
                        $(underlineI).appendTo($(underlineButton));
                        $(underlineButton).css("padding", "7px 7px 7px 7px");
                        $(underlineButton).css("border", "1px solid rgb(255,255,255)");
                        $(underlineButton).css("width", "23px");
                        $(underlineButton).css("-webkit-border-radius", "5px");
                        $(underlineButton).css("-moz-border-radius", "5px");
                        $(underlineButton).css("border-radius", "5px");
                        $(underlineButton).css("float", "left");
                        $(underlineButton).css("margin-left", "10px");
                        $(underlineButton).css("cursor", "pointer");

                        $(underlineButton).addClass("underlineButton");
                        $(underlineButton).addClass("systemDIV");
                        $(underlineButton).appendTo($(fontControls));

                        var fontSelectorHolder = $("<div>");
                        $(fontSelectorHolder).addClass("systemDIV");
                        $(fontSelectorHolder).css("margin-left", "10px");
                        $(fontSelectorHolder).css("padding-top", "5px");
                        $(fontSelectorHolder).css("padding-bottom", "4px");
                        $(fontSelectorHolder).css("float", "left");
                        var fontSelector = $("<select>");
                        $(fontSelector).addClass("fontSelector");

                        var currentFontFamily = $(this).css("font-family");
                        var currentFontFamily = currentFontFamily.substr(currentFontFamily.indexOf("'") + 1);
                        currentFontFamily = currentFontFamily.substr(0, currentFontFamily.indexOf("'"));

                        var fontsLinksList = $("link.fontsList");
                        for (var x = 0; x < $(fontsLinksList).length; x++) {
                            var linkElement = fontsLinksList[x];
                            var name = $(linkElement).attr("data-name");
                            var optionElement = $("<option>");
                            if (name == currentFontFamily) {
                                $(optionElement).attr("selected", "selected");
                            }
                            $(optionElement).val(name);
                            $(optionElement).html(name);
                            $(optionElement).appendTo($(fontSelector));

                        }
                        $(fontSelector).appendTo($(fontSelectorHolder));
                        $(fontSelectorHolder).appendTo($(fontControls));

                        var fontSizeSelectorHolder = $("<div>");
                        $(fontSizeSelectorHolder).addClass("systemDIV");
                        $(fontSizeSelectorHolder).css("margin-left", "10px");
                        $(fontSizeSelectorHolder).css("padding-top", "5px");
                        $(fontSizeSelectorHolder).css("padding-bottom", "4px");
                        $(fontSizeSelectorHolder).css("float", "left");
                        var fontSizeSelector = $("<select>");
                        $(fontSizeSelector).addClass("fontSizeSelector");

                        var currentFontSize = $(this).css("font-size");
                        currentFontSize = currentFontSize.replace("px", "");
                        currentFontSize = currentFontSize.replace("x", "");

                        for (var x = 0; x < 31; x++) {
                            var optionElement = $("<option>");
                            if (x == currentFontSize) {
                                $(optionElement).attr("selected", "selected");
                            }
                            $(optionElement).val(x + "px");
                            $(optionElement).html(x + "px");
                            $(optionElement).appendTo($(fontSizeSelector));

                        }
                        $(fontSizeSelector).appendTo($(fontSizeSelectorHolder));
                        $(fontSizeSelectorHolder).appendTo($(fontControls));

                        var alignLeftButton = $("<div>");
                        var alignLeftI = $("<i>");
                        $(alignLeftI).addClass("icon-align-left");
                        $(alignLeftI).appendTo($(alignLeftButton));
                        $(alignLeftButton).css("padding", "7px 7px 7px 7px");
                        $(alignLeftButton).css("border", "1px solid rgb(255,255,255)");
                        $(alignLeftButton).css("width", "23px");
                        $(alignLeftButton).css("-webkit-border-radius", "5px");
                        $(alignLeftButton).css("-moz-border-radius", "5px");
                        $(alignLeftButton).css("border-radius", "5px");
                        $(alignLeftButton).css("float", "left");
                        $(alignLeftButton).css("cursor", "pointer");
                        $(alignLeftButton).css("clear", "both");
                        $(alignLeftButton).addClass("alignLeftButton");
                        $(alignLeftButton).addClass("systemDIV");
                        $(alignLeftButton).css("margin-top", "10px");
                        $(alignLeftButton).appendTo($(fontControls));

                        var alignCenterButton = $("<div>");
                        var alignCenterI = $("<i>");
                        $(alignCenterI).addClass("icon-align-center");
                        $(alignCenterI).appendTo($(alignCenterButton));
                        $(alignCenterButton).css("padding", "7px 7px 7px 7px");
                        $(alignCenterButton).css("border", "1px solid rgb(255,255,255)");
                        $(alignCenterButton).css("width", "23px");
                        $(alignCenterButton).css("-webkit-border-radius", "5px");
                        $(alignCenterButton).css("-moz-border-radius", "5px");
                        $(alignCenterButton).css("border-radius", "5px");
                        $(alignCenterButton).css("float", "left");
                        $(alignCenterButton).css("cursor", "pointer");
                        $(alignCenterButton).addClass("alignCenterButton");
                        $(alignCenterButton).addClass("systemDIV");
                        $(alignCenterButton).css("margin-left", "10px");
                        $(alignCenterButton).css("margin-top", "10px");
                        $(alignCenterButton).appendTo($(fontControls));

                        var alignRightButton = $("<div>");
                        var alignRightI = $("<i>");
                        $(alignRightI).addClass("icon-align-right");
                        $(alignRightI).appendTo($(alignRightButton));
                        $(alignRightButton).css("padding", "7px 7px 7px 7px");
                        $(alignRightButton).css("border", "1px solid rgb(255,255,255)");
                        $(alignRightButton).css("width", "23px");
                        $(alignRightButton).css("-webkit-border-radius", "5px");
                        $(alignRightButton).css("-moz-border-radius", "5px");
                        $(alignRightButton).css("border-radius", "5px");
                        $(alignRightButton).css("float", "left");
                        $(alignRightButton).css("cursor", "pointer");
                        $(alignRightButton).addClass("alignRightButton");
                        $(alignRightButton).addClass("systemDIV");
                        $(alignRightButton).css("margin-left", "10px");
                        $(alignRightButton).css("margin-top", "10px");
                        $(alignRightButton).appendTo($(fontControls));

                        var alignJustifyButton = $("<div>");
                        var alignJustifyI = $("<i>");
                        $(alignJustifyI).addClass("icon-align-justify");
                        $(alignJustifyI).appendTo($(alignJustifyButton));
                        $(alignJustifyButton).css("padding", "7px 7px 7px 7px");
                        $(alignJustifyButton).css("border", "1px solid rgb(255,255,255)");
                        $(alignJustifyButton).css("width", "23px");
                        $(alignJustifyButton).css("-webkit-border-radius", "5px");
                        $(alignJustifyButton).css("-moz-border-radius", "5px");
                        $(alignJustifyButton).css("border-radius", "5px");
                        $(alignJustifyButton).css("float", "left");
                        $(alignJustifyButton).css("cursor", "pointer");
                        $(alignJustifyButton).addClass("alignJustifyButton");
                        $(alignJustifyButton).addClass("systemDIV");
                        $(alignJustifyButton).css("margin-left", "10px");
                        $(alignJustifyButton).css("margin-top", "10px");
                        $(alignJustifyButton).appendTo($(fontControls));

                        var linkButton = $("<div>");
                        var linkI = $("<i>");
                        $(linkI).addClass("icon-link");
                        $(linkI).appendTo($(linkButton));
                        $(linkButton).css("padding", "7px 7px 7px 7px");
                        $(linkButton).css("border", "1px solid rgb(255,255,255)");
                        $(linkButton).css("width", "23px");
                        $(linkButton).css("-webkit-border-radius", "5px");
                        $(linkButton).css("-moz-border-radius", "5px");
                        $(linkButton).css("border-radius", "5px");
                        $(linkButton).css("float", "left");
                        $(linkButton).css("cursor", "pointer");
                        $(linkButton).addClass("linkButton");
                        $(linkButton).addClass("systemDIV");
                        $(linkButton).css("margin-left", "10px");
                        $(linkButton).css("margin-top", "10px");
                        $(linkButton).appendTo($(fontControls));

                        var unLinkButton = $("<div>");
                        var unLinkI = $("<i>");
                        $(unLinkI).addClass("icon-unlink");
                        $(unLinkI).appendTo($(unLinkButton));
                        $(unLinkButton).css("padding", "7px 7px 7px 7px");
                        $(unLinkButton).css("border", "1px solid rgb(255,255,255)");
                        $(unLinkButton).css("width", "23px");
                        $(unLinkButton).css("-webkit-border-radius", "5px");
                        $(unLinkButton).css("-moz-border-radius", "5px");
                        $(unLinkButton).css("border-radius", "5px");
                        $(unLinkButton).css("float", "left");
                        $(unLinkButton).css("cursor", "pointer");
                        $(unLinkButton).addClass("unLinkButton");
                        $(unLinkButton).addClass("systemDIV");
                        $(unLinkButton).css("margin-left", "10px");
                        $(unLinkButton).css("margin-top", "10px");
                        $(unLinkButton).appendTo($(fontControls));

                        var fontControlsTop = $(fontControls).clone();
                        $(fontControlsTop).css("top", "-95px");
                        $(fontControlsTop).css("left", "168px");
                        $(fontControlsTop).appendTo($(this));

                        var fontControlsBottom = $(fontControls).clone();
                        $(fontControlsBottom).css("bottom", "-95px");
                        $(fontControlsBottom).css("left", "168px");
                        $(fontControlsBottom).appendTo($(this));


                        i = $("<i>");
                        $(i).addClass("icon-pencil");
                        $(i).css("float", "left");
                        $(i).css("cursor", "pointer");
                        $(i).css("margin-left", "7px");
                        $(i).css("color", "rgb(255, 255, 255)");
                        $(i).appendTo($(divControls));

                        i = $("<i>");
                        $(i).addClass("icon-trash");
                        $(i).css("float", "right");
                        $(i).css("cursor", "pointer");
                        $(i).css("margin-left", "15px");
                        $(i).css("color", "rgb(255, 255, 255)");
                        $(i).appendTo($(divControls));

                        var divControlsTop = $(divControls).clone();
                        $(divControlsTop).css("top", "-33px");
                        $(divControlsTop).appendTo($(this));

                        var divControlsBottom = $(divControls).clone();
                        $(divControlsBottom).css("bottom", "-33px");
                        $(divControlsBottom).appendTo($(this));

                    },
                    stop: function(event, ui) {
                        var element = $(ui.helper);
                        renderElement(element);
                        renderPost($(element), "dragText");
                    }
                })
                        .bind('click', function() {
                            if (!$(drg).is($(this))) {
                                drg = $(this);
                                $(this).focus();
                                $('.editableText', $(this)).focus();
                                console.log("FOCUS~!!");
                            }
                        });

                $('.editableText', $(this)).attr("data-already-clicked", "1");
                $('.editableText', $(this)).mousedown(function(ev) {
                    $(this).parent().draggable('disable');
                }).mouseup(function(ev) {
                    $(this).parent().draggable('enable');
                });


            }

            $(this).resizable({handles: "n, e, s, w, ne, se, sw, nw",
                stop: function(event, ui) {
                    $(ui.draggable).css("position", "relative !important");

                    var element = $(ui.helper);
                    renderElement(element);
                    renderPost($(element), "resizeText");
                }
            });

            $(this).attr("contenteditable", "false");
            $("a:not(.systemDIV)", this).attr("contenteditable", "true");
            $("div:not(.systemDIV)", this).attr("contenteditable", "true");
            $("a.systemDIV", this).attr("contenteditable", "false");
            $("div.systemDIV", this).attr("contenteditable", "false");
            $("div.editableText", this).attr("contenteditable", "true");
        }
        e.stopPropagation();
        e.preventDefault();

    });

// Убирает ссылку
    $(document).on("click", ".icon-unlink", function(e) {
        e.stopPropagation();
        e.preventDefault();

        var prnt = $(this).parent().parent();
        prnt = $(".editableText", prnt);

        var parentElement = getSelectionParentElement();
//        console.log(parentElement);
        var checkedElement = $(parentElement, prnt);
//        console.log(checkedElement);

        if ($(checkedElement).is("a")) {

//            console.log("!!!!");

            var link = $(checkedElement);
            $(link).replaceWith($(link).html());
        }

        return false;

    });

// Не даёт сломать блок
    $(document).on("keydown", ".editableText", function(e) {
        if ($(this).html().length < 1) {
            if (e.keyCode == 46) {
                e.stopPropagation();
                e.preventDefault();
            }
            e.stopPropagation();
        }
    });

// Создание ссылки
    $(document).on("click", ".linkButton", function(e) {
        var prnt = $(this).parent().parent();
        replaceSelection();

        var linkFormMain = createLinkForm($(prnt));

        var containerDiv = $(".containerDiv", linkFormMain);
        var tableDiv = $("<div class='tableDiv systemDIV'>");
        $(tableDiv).css("width", "50%");
//        $(tableDiv).css("height", "0px");
        $(tableDiv).css("margin-left", "auto");
        $(tableDiv).css("margin-right", "auto");
//        $(tableDiv).html("sdfsfdf");    

        var tr1 = $("<div class='tr systemDIV'>");

        var td1 = $("<div class='td systemDIV'>");
        $(td1).css("float", "left");
        $(td1).css("width", "40%");
        $(td1).text("Введите ссылку");
        $(td1).appendTo($(tr1));

        var td2 = $("<div class='td systemDIV'>");
        $(td2).css("float", "left");
        $(td2).css("width", "60%");
        var inputForm = $("<input type='text' id='linkFormValue'>");
        $(inputForm).appendTo($(td2));
        $(td2).appendTo($(tr1));

        $(tr1).appendTo($(tableDiv));


        var tr2 = $("<div class='tr systemDIV'>");

        var td2_1 = $("<div class='td systemDIV'>");
        $(td2_1).css("float", "left");
        $(td2_1).css("width", "40%");
        $(td2_1).appendTo($(tr2));

        var td2_2 = $("<div class='td systemDIV'>");
        $(td2_2).css("float", "left");
        $(td2_2).css("width", "60%");
        var inputForm = $("<button class='saveLinkButton'>Сохранить ссылку и закрыть окно</button>");
        $(inputForm).appendTo($(td2_2));
        $(td2_2).appendTo($(tr2));

        $(tr2).appendTo($(tableDiv));

        $(tableDiv).appendTo($(containerDiv));

        $(linkFormMain).appendTo($("body"));
//        $(linkFormMain).remove();

        e.stopPropagation();
        e.preventDefault();
    });

// Закрывает форму ввода ссылки
    $(document).on("click", ".closeLinkForm", function(e) {
        e.stopPropagation();
        e.preventDefault();
        var aForRemove = $("a#tmpLink");
        $(aForRemove).replaceWith($(aForRemove).html());
        $(this).parents(".linkFormMain").remove();
    });

// Сохраняет значение ссылки и закрывает форму ввода ссылки
    $(document).on("click", ".saveLinkButton", function(e) {
        var linkFormValue = $("#linkFormValue").val();
        var aForEdit = $("a#tmpLink");
        $(aForEdit).removeAttr("id");
        $(aForEdit).attr("href", linkFormValue);

        $(this).parents(".linkFormMain").remove();

        e.stopPropagation();
        e.preventDefault();
    });

// Заглушка
    $(document).on("click", ".fontSizeSelector", function(e) {
        e.stopPropagation();
        e.preventDefault();
    });

// Форматирование текста по левой стороне
    $(document).on("click", ".alignLeftButton", function(e) {

        var prnt = $(this).parent().parent();

        renderPre($(prnt));
        $(prnt).css("text-align", "left");
        renderElement(prnt);
        renderPost(prnt, "textAlign");

        e.stopPropagation();
        e.preventDefault();
    });

// Форматирование текста по центру
    $(document).on("click", ".alignCenterButton", function(e) {
        var prnt = $(this).parent().parent();

        renderPre($(prnt));
        $(prnt).css("text-align", "center");
        renderElement(prnt);
        renderPost(prnt, "textAlign");

        e.stopPropagation();
        e.preventDefault();
    });

// Форматирование текста по правой стороне
    $(document).on("click", ".alignRightButton", function(e) {
        var prnt = $(this).parent().parent();

        renderPre($(prnt));
        $(prnt).css("text-align", "right");
        renderElement(prnt);
        renderPost(prnt, "textAlign");

        e.stopPropagation();
        e.preventDefault();
    });

// Форматирование текста по ширине
    $(document).on("click", ".alignJustifyButton", function(e) {
        var prnt = $(this).parent().parent();

        renderPre($(prnt));
        $(prnt).css("text-align", "justify");
        renderElement(prnt);
        renderPost(prnt, "textAlign");

        e.stopPropagation();
        e.preventDefault();
    });

// Изменение размера текста
    $(document).on("change", ".fontSizeSelector", function(e) {
        var prnt = $(this).parent().parent().parent();

        renderPre($(prnt));
        var fontSize = $(this).val();
        $(prnt).css("line-height", fontSize);
        $(prnt).css("font-size", fontSize);
        renderElement(prnt);
        renderPost(prnt, "textSize");

        e.stopPropagation();
        e.preventDefault();
    });


// Заглушка
    $(document).on("click", ".fontSelector", function(e) {
        e.stopPropagation();
        e.preventDefault();
    });

// Изменение шрифта текста
    $(document).on("change", ".fontSelector", function(e) {
        var prnt = $(this).parent().parent().parent();

        renderPre($(prnt));

        var fontFamily = $(this).val();
        usedFonts.push(fontFamily);
        var tmpVal = "'" + fontFamily + "', " + $("link[data-name='" + fontFamily + "']").attr("data-type");
        $(prnt).css("font-family", tmpVal);

        $(".systemDIV", prnt).css("font-family", "'Open Sans', sans-serif;");

        renderElement(prnt);
        renderPost(prnt, "textFont");

        e.stopPropagation();
        e.preventDefault();
    });

// Курсив 
    $(document).on("click", ".italicButton", function(e) {
        e.stopPropagation();
        var prnt = $(this).parent().parent();
//        var prnt2 = $(".editableText", prnt);
        
        renderPre($(prnt));

        if ($(prnt).css("font-style") == "italic") {
            $(prnt).css("font-style", "normal");
//            $(prnt2).css("font-style", "normal");
            $(".systemDIV", prnt).css("font-style", "normal");
        } else {
            $(prnt).css("font-style", "italic");
//            $(prnt2).css("font-style", "italic");
            $(".systemDIV", prnt).css("font-style", "normal");
        }

        renderElement(prnt);
        renderPost(prnt, "textDecors");

        e.preventDefault();
    });

// Жирный шрифт
    $(document).on("click", ".boldButton", function(e) {
        e.stopPropagation();
        var prnt = $(this).parent().parent();

        renderPre($(prnt));

        if ($(prnt).css("font-weight") == "bold") {
            $(prnt).css("font-weight", "normal");
            $(".systemDIV", prnt).css("font-weight", "normal");
        } else {
            $(prnt).css("font-weight", "bold");
            $(".systemDIV", prnt).css("font-weight", "normal");
        }

        renderElement(prnt);
        renderPost(prnt, "textDecors");

        e.preventDefault();
    });

// Зачёркнутый текст
    $(document).on("click", ".strikeButton", function(e) {
        e.stopPropagation();
        var prnt = $(this).parent().parent();

        renderPre($(prnt));

        var style = $(prnt).css("text-decoration");

        if (style.indexOf("line-through") > -1) {
            var styleLocal = style.replace("line-through", "");
            if (styleLocal.indexOf("underline") < 0) {
                styleLocal = "none";
            } else {
                styleLocal = "underline ";
            }

            $(prnt).css("text-decoration", styleLocal);
            $(".systemDIV", prnt).css("text-decoration", "none");
        } else {
            var styleLocal = "";

            if (style.indexOf("underline") > -1) {
                styleLocal = styleLocal + "underline ";
            }
            styleLocal = styleLocal + " line-through";

            $(prnt).css("text-decoration", styleLocal);
            $(".systemDIV", prnt).css("text-decoration", "none");
        }

        renderElement(prnt);
        renderPost(prnt, "textDecors");

        e.preventDefault();
    });

// Подчёркнутый текст
    $(document).on("click", ".underlineButton", function(e) {
        e.stopPropagation();
        var prnt = $(this).parent().parent();

        var style = $(prnt).css("text-decoration");

        renderPre($(prnt));

        if (style.indexOf("underline") > -1) {
            var styleLocal = style.replace("underline", "");
            if (styleLocal.indexOf("line-through") < 0) {
                styleLocal = "none";
            } else {
                styleLocal = "line-through ";
            }

            $(prnt).css("text-decoration", styleLocal);
            $(".systemDIV", prnt).css("text-decoration", "none");
        } else {
            var styleLocal = "";

            if (style.indexOf("line-through") > -1) {
                styleLocal = styleLocal + "line-through ";
            }
            styleLocal = styleLocal + " underline";

            $(prnt).css("text-decoration", styleLocal);
            $(".systemDIV", prnt).css("text-decoration", "none");
        }

        renderElement(prnt);
        renderPost(prnt, "textDecors");

        e.preventDefault();
    });

    $(document).on("click", ".closeUploadFormDiv", function(e) {
        $(this).parent().parent().remove();
        e.stopPropagation();
        e.preventDefault();
    });

    $(document).on("click", ".icon-trash", function(e) {
        $(this).parents(".imageHolder").remove();
        if ($(this).parent().parent().hasClass("textHolder")) {
            $(this).parent().parent().remove();
        }

    });

    $(document).on("click", ".previewCloseAndDelete", function(e) {
        deleteFile($(this));
    });

    $(document).on("click", ".preview img", function(e) {

        var img = $("<img>");

        $(img).css({
            position: 'absolute',
            top: 0,
            left: 0,
            width: 'inherit',
            height: 'inherit'
        });

        $(img).attr("src", $(this).attr("src"));
        $(divForFile).html($(img));

        renderElement($(img), 'inherit', 'inherit');

        $(this).parent().parent().parent().parent().parent().remove();
    }
    );

    $(document).on("click", ".icon-pencil", function(e) {

        $(".uploadFormDivMain").remove();

        var prnt = $(this).parent().parent();

        if ($(prnt).hasClass("ui-wrapper")) {
            prnt = $(this).parent().parent().parent();
        }

        var prntTop = $(prnt).offset().top;

        if (prntTop > 200) {
            prntTop = prntTop - 200;
        }

        if (prntTop < 100) {
            prntTop = 100;
        }

        if ($(prnt).hasClass("imageHolder")) {

            divForFile = $(prnt);

            var uploadFormDivMain = $("<div>");
            $(uploadFormDivMain).addClass("uploadFormDivMain");
            $(uploadFormDivMain).addClass("systemDIV");
            $(uploadFormDivMain).css("top", prntTop);

            var uploadFormDiv = $("<div>");
            $(uploadFormDiv).addClass("uploadFormDiv");
            $(uploadFormDiv).addClass("systemDIV");
            $(uploadFormDiv).appendTo($(uploadFormDivMain));

            var close = $("<div>");
            $(close).addClass("closeUploadFormDiv");
            $(close).addClass("systemDIV");
            var i = $("<i>");
            $(i).addClass("icon-cancel");
            $(i).appendTo($(close));
            $(close).appendTo($(uploadFormDiv));

            var uploadFormDivDropbox = $("<div>");
            $(uploadFormDivDropbox).addClass("uploadFormDivDropbox");
            $(uploadFormDivDropbox).addClass("systemDIV");
            var spanText = $("<div>");
            $(spanText).addClass("spanInfoText");
            $(spanText).addClass("systemDIV");
            $(spanText).html("Drop images here to upload. <br /><i>(they will only be visible to you)</i>");
            $(spanText).appendTo($(uploadFormDivDropbox));
            $(uploadFormDivDropbox).appendTo($(uploadFormDiv));

            loadFilesForLandings($(uploadFormDivDropbox));

            $(uploadFormDivDropbox).filedrop({
                // The name of the $_FILES entry:
                paramname: 'pic',
                maxfiles: 5,
                maxfilesize: 2,
                url: '/khartn_cms/components/khartn/landings.create.new.editor/post_file.php',
                uploadFinished: function(i, file, response) {
                    $.data(file).find('.previewCloseAndDelete').attr("data-file", response.status);
                },
                error: function(err, file) {
                    switch (err) {
                        case 'BrowserNotSupported':
                            alert('Your browser does not support HTML5 file uploads!');
                            break;
                        case 'TooManyFiles':
                            alert('Too many files! Please select 5 at most! (configurable)');
                            break;
                        case 'FileTooLarge':
                            alert(file.name + ' is too large! Please upload files up to 2mb (configurable).');
                            break;
                        default:
                            break;
                    }
                },
                // Called before each upload is started
                beforeEach: function(file) {
                    if (!file.type.match(/^image\//)) {
                        alert('Only images are allowed!');

                        // Returning false will cause the
                        // file to be rejected
                        return false;
                    }
                },
                uploadStarted: function(i, file, len) {
                    createImage(file);
                },
                progressUpdated: function(i, file, progress) {
                    $.data(file).find('.progress').width(progress);
                }

            });

            function createImage(file) {

                var preview = $(template);
                var image = $('img', preview);
                $(image).addClass('systemDIV');

                var reader = new FileReader();

                image.width = 100;
                image.height = 100;

                reader.onload = function(e) {

                    // e.target.result holds the DataURL which
                    // can be used as a source of the image:

                    image.attr('src', e.target.result);
                };

                // Reading the file as a DataURL. When finished,
                // this will trigger the onload function above:
                reader.readAsDataURL(file);

                preview.appendTo($(uploadFormDivDropbox));

                // Associating a preview container
                // with the file, using jQuery's $.data():

                $.data(file, preview);
            }


            $(uploadFormDivMain).appendTo($("body"));
        } else {
            $(prnt).trigger("click");
            $(prnt).focus();
        }
    });
}

function deleteFile(divHolder) {

    var fileId = $(divHolder).attr("data-file");

    $.post("/khartn_cms/components/khartn/landings.create.new.editor/delete_users_file.php",
            {
                fileId: fileId
            },
    function(data) {
        $(divHolder).parent().remove();
    }, "text");
}

function loadFilesForLandings(dropbox) {
    $.getJSON("/khartn_cms/components/khartn/landings.create.new.editor/get_files_for_user.php")
            .done(function(data) {
                $.each(data.status, function(i, item) {
                    var preview = $(template);
                    var image = $('img', preview);
                    var previewCloseAndDelete = $('.previewCloseAndDelete', preview);
                    $(previewCloseAndDelete).attr("data-file", item.file_id);

                    image.width = 100;
                    image.height = 100;

                    image.attr('src', item.file_path);

                    $(image).addClass('systemDIV');

                    preview.appendTo($(dropbox));

                });
            });
}


var template = '<div class="preview systemDIV">' +
        '<div class="previewCloseAndDelete systemDIV">' +
        '<i class="icon-cancel"></i>' +
        '</div>' +
        '<span class="imageHolder systemDIV">' +
        '<img />' +
        '</span>' +
        '</div>';

function cleanDragAndResize() {

    $(lastSelectedElement).css("z-index", lastSelectedElementsZindex);

    $("div.ui-draggable").css("border", "none");
    $("img.ui-draggable").css("border", "none");
    $("div.divControls").remove();
    $("div.fontControls").remove();
    $("div").removeAttr("contenteditable");
    $("img").removeAttr("contenteditable");
    $("a").removeAttr("contenteditable");

    try {

        var imageWrapperS = $(".imageHolder");
        var imageWrapperSLength = $(".imageHolder").length;
        for (var x = 0; x < imageWrapperSLength; x++) {
            var imageWrapperElement = imageWrapperS[x];
            var width = $('img', imageWrapperElement).css("width");
            var height = $('img', imageWrapperElement).css("height");
            $(imageWrapperElement).css("height", height);
            $(imageWrapperElement).css("width", width);
//            renderElement(imageWrapperElement);
        }


        $("div.ui-draggable").draggable("destroy");
        $("img.ui-draggable").draggable("destroy");
    } catch (e) {
    }

    try {
        $("div.ui-resizable").resizable("destroy");
        $("img.ui-resizable").resizable("destroy");
    } catch (e) {
    }

}

var generateUid = function(separator) {
    /// <summary>
    ///    Creates a unique id for identification purposes.
    /// </summary>
    /// <param name="separator" type="String" optional="true">
    /// The optional separator for grouping the generated segmants: default "-".    
    /// </param>

    var delim = separator || "";
//                    var delim = separator || "-";

    function S4() {
        return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
    }

    return (S4() + S4() + delim + S4() + delim + S4() + delim + S4() + delim + S4() + S4() + S4());
};

function getNeededStyles(element, elementWidthInput, elementHeightInput) {

    var resultArray = [];
    var cssObject = {};

    var elementPosition = $(element).css("position");
    var elementTop = $(element).css("top");
    var elementLeft = $(element).css("left");

    if (elementWidthInput === undefined) {
        var elementWidth = $(element).css("width");
    } else {
        elementWidth = elementWidthInput;
    }

    if (elementHeightInput === undefined) {
        var elementHeight = $(element).css("height");
    } else {
        elementHeight = elementHeightInput;
    }

    var fontWeight = $(element).css("font-weight");
    var fontStyle = $(element).css("font-style");
    var fontFamily = $(element).css("font-family");
    var fontSize = $(element).css("font-size");

    var textDecoration = $(element).css("text-decoration");
    if (textDecoration !== undefined) {
        textDecoration = textDecoration.replace("solid", "");
        textDecoration = textDecoration.replace(/rgb(.*)/, "");
    }

    var textTransform = $(element).css("text-transform");
    var textAlign = $(element).css("text-align");
    var zIndex = $(element).css("z-index");
    var overflowX = $(element).css("overflow-x");
    var overflowY = $(element).css("overflow-y");
    var lineHeight = $(element).css("line-height");
    var backgroundImage = $(element).css("background-image");
    var color = $(element).css("color");
    var backgroundСolor = $(element).css("background-color");
    var padding = $(element).css("padding");
    var border = $(element).css("border");
    var borderRadius = $(element).css("border-radius");

    var content = "";
    content = content + "position: " + elementPosition + "; ";
    content = content + "top: " + elementTop + "; ";
    content = content + "left: " + elementLeft + "; ";
    content = content + "width: " + elementWidth + "; ";
    content = content + "height: " + elementHeight + "; ";
    content = content + "font-weight: " + fontWeight + "; ";
    content = content + "font-style: " + fontStyle + "; ";
    content = content + "text-decoration: " + textDecoration + "; ";
    content = content + "font-family: " + fontFamily + "; ";
    content = content + "font-size: " + fontSize + "; ";
    content = content + "text-transform: " + textTransform + "; ";
    content = content + "z-index: " + zIndex + "; ";
    content = content + "overflow-x: " + overflowX + "; ";
    content = content + "overflow-y: " + overflowY + "; ";
    content = content + "line-height: " + lineHeight + "; ";
    content = content + "background-image: " + backgroundImage + "; ";
    content = content + "color: " + color + "; ";
    content = content + "text-align: " + textAlign + "; ";
    content = content + "background-color: " + backgroundСolor + "; ";
    content = content + "border: " + border + "; ";
    content = content + "border-radius: " + borderRadius + "; ";
    content = content + "padding: " + padding + "; ";

    cssObject = {
        "position": elementPosition,
        "top": elementTop,
        "left": elementLeft,
        "width": elementWidth,
        "height": elementHeight,
        "font-weight": fontWeight,
        "font-style": fontStyle,
        "text-decoration": textDecoration,
        "font-family": fontFamily,
        "font-size": fontSize,
        "text-transform": textTransform,
        "z-index": zIndex,
        "overflow-x": overflowX,
        "overflow-y": overflowY,
        "line-height": lineHeight,
        "background-image": backgroundImage,
        "color": color,
        "text-align": textAlign,
        "background-color": backgroundСolor,
        "border": border,
        "border-radius": borderRadius,
        "padding": padding,
    }

    resultArray[0] = content;
    resultArray[1] = cssObject;

    return resultArray;

}

function renderElement(element, elementWidthInput, elementHeightInput) {

    var styleId = "";
    var styleIdNew = "";

    if ($(element).attr("data-styleId") !== undefined) {
        styleId = $(element).attr("data-styleId");
    }

    styleIdNew = generateUid("");
    $(element).attr("data-styleId", "styles-" + styleIdNew);

    var styleElement = $("<style>");
    $(styleElement).attr("id", "styles-" + styleIdNew);
    var content = "";
    content = content + "." + "styles-" + styleIdNew + " { ";

    var stylesText = getNeededStyles(element, elementWidthInput, elementHeightInput);
    content = content + stylesText[0];
    content = content + " } ";
    $(styleElement).html(content);

    $("head").append($(styleElement).clone().wrap('<div>').parent().html());

    $(element).attr("style", "");
    if (styleId != "") {
        $("style#" + styleId).remove();
    }

    var classVar = $(element).attr('class');
    if (classVar !== undefined) {
        classVar = $(element).attr('class').replace(/\bstyles-\S\w*\b/g, '');
        classVar.replace("  ", "");
//        $('element').attr('className').replace(/\bclass-\d+\b/g, '')
    }
    $(element).attr('class', classVar);
    $(element).attr("data-styleId", "styles-" + styleIdNew);
    $(element).addClass("styles-" + styleIdNew);
    $(element).removeAttr("id");



}
var elementLast;
function renderPre(element) {

    if (!$(elementLast).is(element)) {
        elementLast = element;
        undoCounter++;
    }

    undoElementsArrayPre[undoCounter] = $(element);
    undoElementsStylesArrayPre[undoCounter] = getNeededStyles(element);
    undoOperations[undoCounter] = "";

}

function renderPost(element, operation) {

    undoCounter++;

    undoElementsArrayPre[undoCounter ] = $(element);
    var styles = getNeededStyles(element);
    undoElementsStylesArrayPre[undoCounter ] = styles;
    undoOperations[undoCounter ] = operation;

}

function undo() {

    cleanDragAndResize();


    console.log("undoCounter");
    console.log(undoCounter);

    console.log("undoElementsArrayPre");
    console.log(undoElementsArrayPre);

    console.log("undoElementsStylesArrayPre");
    console.log(undoElementsStylesArrayPre);

    console.log("undoOperations");
    console.log(undoOperations);


    console.log("++++++++++++++++++++++++++++++++");



    if (undoCounter > 0) {

        if (undoOperations[undoCounter] == "") {
            undoCounter = undoCounter - 1;
            undoElementsArrayPre.pop();
            undoElementsStylesArrayPre.pop();
            undoOperations.pop();
        }

        if (undoOperations[undoCounter] == "textDecors") {
            undoCounter = undoCounter - 1;

            undoElementsArrayPre.pop();
            undoElementsStylesArrayPre.pop();
            undoOperations.pop();


            var element = undoElementsArrayPre[undoCounter];
            var oldCssObj = undoElementsStylesArrayPre[undoCounter][1];

            $(document).scrollTop($(element).offset().top);

            $(element).css(oldCssObj);
        }

        if (undoOperations[undoCounter] == "textFont") {
            undoCounter = undoCounter - 1;

            undoElementsArrayPre.pop();
            undoElementsStylesArrayPre.pop();
            undoOperations.pop();


            var element = undoElementsArrayPre[undoCounter];
            var oldCssObj = undoElementsStylesArrayPre[undoCounter][1];

            $(document).scrollTop($(element).offset().top);

            $(element).css(oldCssObj);
        }

        if (undoOperations[undoCounter] == "textSize") {
            undoCounter = undoCounter - 1;

            undoElementsArrayPre.pop();
            undoElementsStylesArrayPre.pop();
            undoOperations.pop();


            var element = undoElementsArrayPre[undoCounter];
            var oldCssObj = undoElementsStylesArrayPre[undoCounter][1];

            $(document).scrollTop($(element).offset().top);

            $(element).css(oldCssObj);
        }

        if (undoOperations[undoCounter] == "textAlign") {
            undoCounter = undoCounter - 1;

            undoElementsArrayPre.pop();
            undoElementsStylesArrayPre.pop();
            undoOperations.pop();


            var element = undoElementsArrayPre[undoCounter];
            var oldCssObj = undoElementsStylesArrayPre[undoCounter][1];

            $(document).scrollTop($(element).offset().top);

            $(element).css(oldCssObj);
        }

        if (undoOperations[undoCounter] == "dragText") {
            undoCounter = undoCounter - 1;

            undoElementsArrayPre.pop();
            undoElementsStylesArrayPre.pop();
            undoOperations.pop();


            var element = undoElementsArrayPre[undoCounter];
            var oldCssObj = undoElementsStylesArrayPre[undoCounter][1];

            $(document).scrollTop($(element).offset().top);

            $(element).css(oldCssObj);
        }

        if (undoOperations[undoCounter] == "resizeText") {
            undoCounter = undoCounter - 1;

            undoElementsArrayPre.pop();
            undoElementsStylesArrayPre.pop();
            undoOperations.pop();


            var element = undoElementsArrayPre[undoCounter];
            var oldCssObj = undoElementsStylesArrayPre[undoCounter][1];

            $(document).scrollTop($(element).offset().top);

            $(element).css(oldCssObj);
        }

        if (undoOperations[undoCounter] == "resizeImg") {
            undoCounter = undoCounter - 1;

            undoElementsArrayPre.pop();
            undoElementsStylesArrayPre.pop();
            undoOperations.pop();


//            console.log("undoCounter");
//            console.log(undoCounter);
//
//            console.log("undoElementsArrayPre");
//            console.log(undoElementsArrayPre);
//
//            console.log("undoElementsStylesArrayPre");
//            console.log(undoElementsStylesArrayPre);
//
//            console.log("undoOperations");
//            console.log(undoOperations);
//
//
//            console.log("*****************************");



            var element = undoElementsArrayPre[undoCounter];
            var oldCssObj = undoElementsStylesArrayPre[undoCounter][1];

            $(document).scrollTop($(element).offset().top);

            $(element).css(oldCssObj);
            $("img", element).css("width", $(element).css("width"));
            $("img", element).css("height", $(element).css("height"));
        }

        if (undoOperations[undoCounter] == "dragImg") {
            undoCounter = undoCounter - 1;

            console.log("dragImg");
            undoElementsArrayPre.pop();
            undoElementsStylesArrayPre.pop();
            undoOperations.pop();

            var element = undoElementsArrayPre[undoCounter];
            var oldCssObj = undoElementsStylesArrayPre[undoCounter][1];

            $(document).scrollTop($(element).offset().top);

            $(element).css(oldCssObj);
            $(element).css("top", $(element).css("top"));
            $(element).css("left", $(element).css("left"));
            $(element).css("border", "none");
        }

    }
}


function replaceSelection(html) {
    var sel, range, node;

    if (typeof window.getSelection != "undefined") {
        // IE 9 and other non-IE browsers
        sel = window.getSelection();

        // Test that the Selection object contains at least one Range
        if (sel.getRangeAt && sel.rangeCount) {
            // Get the first Range (only Firefox supports more than one)
            range = window.getSelection().getRangeAt(0);
            var content = range.cloneContents();

            var span = document.createElement('SPAN');


            span.appendChild(content);
            var htmlContent = span.innerHTML;
            htmlContent = "<a href='#' id='tmpLink'>" + htmlContent + "</a>";

//range.insertNode(span);
//            alert(htmlContent);

            range.deleteContents();

            // Create a DocumentFragment to insert and populate it with HTML
            // Need to test for the existence of range.createContextualFragment
            // because it's non-standard and IE 9 does not support it
            if (range.createContextualFragment) {
                node = range.createContextualFragment(htmlContent);
            } else {
                // In IE 9 we need to use innerHTML of a temporary element
                var div = document.createElement("div"), child;
                div.innerHTML = htmlContent;
                node = document.createDocumentFragment();
                while ((child = div.firstChild)) {
                    node.appendChild(child);
                }
            }
            range.insertNode(node);
        }
    } else if (document.selection && document.selection.type != "Control") {
        // IE 8 and below
        range = document.selection.createRange();
        range.pasteHTML(htmlContent);
    }
}

function getSelectionParentElement() {
    var parentEl = null, sel;
    if (window.getSelection) {
        sel = window.getSelection();
        if (sel.rangeCount) {
            parentEl = sel.getRangeAt(0).commonAncestorContainer;
            if (parentEl.nodeType != 1) {
                parentEl = parentEl.parentNode;
            }
        }
    } else if ((sel = document.selection) && sel.type != "Control") {
        parentEl = sel.createRange().parentElement();
    }
    return parentEl;
}

function createLinkForm(prnt) {

    var prntTop = $(prnt).offset().top;

    if (prntTop > 200) {
        prntTop = prntTop - 200;
    }

    if (prntTop < 100) {
        prntTop = 100;
    }

    var linkFormMain = $("<div>");
    $(linkFormMain).addClass("linkFormMain");
    $(linkFormMain).addClass("systemDIV");
    $(linkFormMain).css("top", prntTop);
    $(linkFormMain).css("position", "absolute");
    $(linkFormMain).css("width", "100%");
    $(linkFormMain).css("z-index", "2000000");


    var linkForm = $("<div>");
    $(linkForm).addClass("linkForm");
    $(linkForm).addClass("systemDIV");
    $(linkForm).appendTo($(linkFormMain));

    var close = $("<div>");
    $(close).addClass("closeLinkForm");
    $(close).addClass("systemDIV");
    var i = $("<i>");
    $(i).addClass("icon-cancel");
    $(i).appendTo($(close));
    $(close).appendTo($(linkForm));

    var containerDIv = $("<div class='containerDiv systemDIV'>");
    $(containerDIv).appendTo($(linkForm));


    return $(linkFormMain);

}
<!DOCTYPE html>
<html lang="ru">
    <head>
        <?php
        APPLICATION::printTitle();
        ?>

        <meta charset="UTF-8">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="viewport" content="width=device-width">

        <script>
            (function(i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r;
                i[r] = i[r] || function() {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
                a = s.createElement(o),
                        m = s.getElementsByTagName(o)[0];
                a.async = 1;
                a.src = g;
                m.parentNode.insertBefore(a, m)
            })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

            ga('create', 'UA-2766332-13', 'landings.pw');
            ga('send', 'pageview');

        </script>

        <?php
        APPLICATION::addCSS("/khartn_cms/templates/editor/css/main.css");
        APPLICATION::addCSS("/khartn_cms/templates/editor/fontello/css/fontello.css");
        ?>

        <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,800,700,600,300&amp;subset=latin,cyrillic' rel='stylesheet' type='text/css' />

        <!--[if lt IE 9]>
            <script type="text/javascript" src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->

        <script type="text/javascript" src="/khartn_cms/templates/editor/js/jquery/jquery-2.0.3.js"></script>

        <?php
//        APPLICATION::addJS("/khartn_cms/templates/editor/js/jquery/jquery-1.10.2.min.js");
        APPLICATION::addJS("/khartn_cms/templates/editor/js/main/main.js");
        APPLICATION::addJS("/khartn_cms/templates/editor/js/jquery.filedrop.js");

        APPLICATION::addCSS("/khartn_cms/templates/editor/js/jquery-ui/css/smoothness/jquery-ui-1.10.3.custom.css");
        APPLICATION::addJS("/khartn_cms/templates/editor/js/jquery-ui/js/jquery-ui-1.10.3.custom.js");
        
        APPLICATION::addCSS("/khartn_cms/third-party-libs/colorpicker/css/colorpicker.css");
        APPLICATION::addJS("/khartn_cms/third-party-libs/colorpicker/js/colorpicker.js");
//        APPLICATION::addJS("/khartn_cms/third-party-libs/colorpicker/js/eye.js");
//        APPLICATION::addJS("/khartn_cms/third-party-libs/colorpicker/js/utils.js");
//        APPLICATION::addJS("/khartn_cms/third-party-libs/colorpicker/js/layout.js?ver=1.0.2");
        

        APPLICATION::printCSSURLs();
        APPLICATION::printJSURLs();
        ?>

    </head>
    <body>
        <div class="body-wrapper">
            <div class="header-wrapper">
                <div class="row">
                    <header>
                        <div class="home-logo-wrapper">
                            <a href="/">
                                <i class="icon-home" style="z-index: 1;"></i>
                            </a>
                        </div>
                        <div class="nav-wrapper">
                            <a href="/ru/new/" class="redButton" id="savePageButton">
                                <?= Dict::$save ?> <i class="icon-floppy"></i>
                            </a>
                        </div>

                        <div class="langs">
                            <div class="ru">
                                <a href="/ru/"><img src="/khartn_cms/templates/land/images/flags/Russia_Flag.png" /> Русский</a>
                            </div>
                            <div class="eng">
                                <a href="/eng/"><img src="/khartn_cms/templates/land/images/flags/USA_Flag.png" /> English</a>
                            </div>
                        </div>

                        <div class="undoButton">
                            <i class="icon-ccw"></i>
                        </div>
                    </header>
                </div>
            </div>
            <div class="delimiter-one">

            </div>
            <aside class="left-side-wrapper-main">
                <div class="left-side-wrapper">
                    <ul class="left-side-vertical-list">
                        <li>
                            <a href="#" id="toogleElementsList">
                                <i class="icon-puzzle"></i>
                            </a>
                            <div class="popup">
                                <i class="icon-left-dir"></i>
                                <span class="popupText">
                                    Меню элементов
                                </span>
                            </div>
                        </li>
                        <li>
                            <a href="#" id="toogleSettingsList" >
                                <i class="icon-cog-alt"></i>
                            </a>
                            <div class="popup">
                                <i class="icon-left-dir"></i>
                                <span class="popupText">
                                    Меню настроек
                                </span>
                            </div>
                        </li>
                        <li>
                            <a href="#" id="toogleSettingsList" >
                                <i class="icon-code"></i>
                            </a>
                            <div class="popup">
                                <i class="icon-left-dir"></i>
                                <span class="popupText">
                                    Код страницы
                                </span>
                            </div>
                        </li>
                    </ul>

                </div>

                <div id="elementsListHolder" class="left-side-slide-absolute">
                    <div class="left-side-slide-wrapper">
                        <div class="holder">
                            <a href="#" class="add-text-action">
                                <i class="icon-text-width"></i>
                            </a>
                            <div class="popup">
                                <i class="icon-left-dir"></i>
                                <span class="popupText">
                                    Текст
                                </span>
                            </div>
                        </div>
                        <div class="holder">
                            <a href="#" class="add-image-action">
                                <i class="icon-picture"></i>
                            </a>
                            <div class="popup">
                                <i class="icon-left-dir"></i>
                                <span class="popupText">
                                    Картинка
                                </span>
                            </div>
                        </div>
                        <div class="holder">
                            <a href="#" class="clean-drag-and-resize">
                                <i class="icon-block"></i>
                            </a>
                            <div class="popup">
                                <i class="icon-left-dir"></i>
                                <span class="popupText">
                                    Сброс выделения
                                </span>
                            </div>
                        </div>
                    </div>
                </div>

                <div id="settingsListHolder" class="left-side-slide-absolute">
                    <div class="left-side-slide-wrapper">

                        <div class="holder">
                            <?= Dict::$background ?>
                        </div>
                        
                        <div class="holder">
                            <?= Dict::$color?>
                            <div class="backgroundColorPickerButton">
                                
                            </div>
                        </div>
                        
                        <div class="holder">
                            <?= Dict::$bgColorOpacity?>
                            <div class="backgroundColorOpacity">
                                <input type="text" value="100%" maxlength="4" size="4" id="backgroundColorOpacity"/>
                            </div>
                        </div>
                        
                        <div class="holder">
                            <?= Dict::$textColor?>
                            <div class="textColorPickerButton">
                                
                            </div>
                        </div>
                    </div>
                </div>
            </aside>



            <!--End of header (since start of page)-->
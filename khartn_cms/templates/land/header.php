<!DOCTYPE html>
<html lang="ru">
    <head>
        <?php
        APPLICATION::printTitle();
        ?>

        <meta charset="UTF-8">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="viewport" content="width=device-width">

        <script>
            (function(i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r;
                i[r] = i[r] || function() {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
                a = s.createElement(o),
                        m = s.getElementsByTagName(o)[0];
                a.async = 1;
                a.src = g;
                m.parentNode.insertBefore(a, m)
            })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

            ga('create', 'UA-2766332-13', 'landings.pw');
            ga('send', 'pageview');

        </script>


        <?php
        APPLICATION::addCSS("/khartn_cms/templates/land/css/main.css");
        APPLICATION::addCSS("/khartn_cms/templates/land/css/footer.css");
        APPLICATION::addCSS("/khartn_cms/templates/editor/css/styles_for_call.css");
        APPLICATION::addCSS("/khartn_cms/templates/land/fontello/css/fontello.css");
        ?>

        <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,800,700,600,300&amp;subset=latin,cyrillic' rel='stylesheet' type='text/css' />

        <!--[if lt IE 9]>
            <script type="text/javascript" src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->


        <script type="text/javascript" src="/khartn_cms/templates/land/js/jquery/jquery-2.0.3.js"></script>
        <?php
//        APPLICATION::addJS("/khartn_cms/templates/land/js/jquery/jquery-1.10.2.min.js");        
        APPLICATION::addCSS("/khartn_cms/templates/editor/js/jquery-ui/css/smoothness/jquery-ui-1.10.3.custom.css");
        APPLICATION::addJS("/khartn_cms/templates/editor/js/jquery-ui/js/jquery-ui-1.10.3.custom.js");
        APPLICATION::addJS("/khartn_cms/templates/land/js/main/main.js");
        APPLICATION::addJS("/khartn_cms/templates/editor/js/main/main_for_call.js");
        APPLICATION::addJS("/khartn_cms/templates/editor/js/jquery.filedrop.js");

        APPLICATION::printCSSURLs();
        APPLICATION::printJSURLs();
        ?>

    </head>
    <body>
        <div class="body-wrapper">
            <div class="header-wrapper">
                <div class="row">
                    <header>
                        <div class="logo-wrapper">
                            <a href="/">
                                <img src="/khartn_cms/templates/land/images/logos/landings.png" >
                            </a>
                        </div>
                        <div class="langs">
                            <div class="ru">
                                <a href="/ru/"><img src="/khartn_cms/templates/land/images/flags/Russia_Flag.png" /> Русский</a>
                            </div>
                            <div class="eng">
                                <a href="/eng/"><img src="/khartn_cms/templates/land/images/flags/USA_Flag.png" /> English</a>
                            </div>
                        </div>
                        <div class="nav-wrapper">
                            <?php
                            APPLICATION::includeComponent("khartn:header.menu");
                            ?>
                        </div>
                    </header>
                </div>
            </div>

            <!--End of header (since start of page)-->
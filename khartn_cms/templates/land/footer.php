<!--            Start of Footer (to end of page) -->
<div class="footer-wrapper">

    <footer>
        <div class="inner">
            <div class="header1">
                Контакты
            </div> 
            <div class="subHeader">
                Вы можете связаться с нами одним из удобным для вас способов.
            </div>
            <div class="contacts">

                <div class="contact">
                    <i class="icon-phone-2"></i> +7 917 870 80 87
                </div>
                <div class="contact">
                    <a href="mailto:info@khartn.name"><i class="icon-mail-5"></i> info@khartn.name </a>
                </div>
                <div class="contact">
                    <a href="/contacts"><i class="icon-mail-5"></i> Отправить сообщение </a>
                </div>
            </div>
            <div class="socSetiLogos">
                <div class="socSetiLogotip">
                    <a href="https://twitter.com/khartnjava" >
                        <i class="icon-twitter-bird twitter" ></i>
                    </a>
                </div>
            </div> 
        </div>
    </footer>
    <div class="footer">
        <div class="inner">
            © 2013 KhArtN. All rights reserved.
        </div> 
    </div>
</div>
</div>

<script type="text/javascript">
    var reformalOptions = {
        project_id: 317981,
        project_host: "landings.reformal.ru",
        tab_orientation: "right",
        tab_indent: "50%",
        tab_bg_color: "#292a2a",
        tab_border_color: "#ff6860",
        tab_image_url: "http://tab.reformal.ru/T9GC0LfRi9Cy0Ysg0Lgg0L%252FRgNC10LTQu9C%252B0LbQtdC90LjRjw==/ff6860/67f786971182e542daedf14ef44be034/right/0/tab.png",
        tab_border_width: 0
    };

    (function() {
        var script = document.createElement('script');
        script.type = 'text/javascript';
        script.async = true;
        script.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'media.reformal.ru/widgets/v3/reformal.js';
        document.getElementsByTagName('head')[0].appendChild(script);
    })();
</script><noscript><a href="http://reformal.ru"><img src="http://media.reformal.ru/reformal.png" /></a><a href="http://landings.reformal.ru">Oтзывы и предложения для landings.pw</a></noscript>

</body>
</html>

$(document).ready(function() {
    $(window).scroll(function() {
        changeHeaderOnScroll();
    });
});

function changeHeaderOnScroll() {

    $(".body-wrapper .header-wrapper .row .nav-wrapper nav ul li a").removeClass("up");
    $(".body-wrapper .header-wrapper .row .nav-wrapper nav ul li a").removeClass("down");
    
    $(".body-wrapper .header-wrapper .row header .langs a").removeClass("up");
    $(".body-wrapper .header-wrapper .row header .langs a").removeClass("down");

    if ($(document).scrollTop() > 10) {
        $(".header-wrapper").css("background-color", "rgba(255, 255, 255, 0.901961)");
        $(".body-wrapper .header-wrapper .row .nav-wrapper nav ul li a").addClass("down");
        $(".body-wrapper .header-wrapper .row header .langs a").addClass("down");

    } else {
        $(".header-wrapper").css("background-color", "rgb(42, 42, 42)");
        $(".body-wrapper .header-wrapper .row .nav-wrapper nav ul li a").addClass("up")
        $(".body-wrapper .header-wrapper .row header .langs a").addClass("up");
//        console.log("fdsfg");

    }

}
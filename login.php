<?php
ini_set("display_errors", "1");
include_once $_SERVER["DOCUMENT_ROOT"] . '/khartn_cms/system/init/header.php';
APPLICATION::setTitle(Dict::$LOG_IN);

?>

<div class = "content-wrapper">
    <?php
    APPLICATION::includeComponent("khartn:login");
    ?>
</div>

<?php
include_once $_SERVER["DOCUMENT_ROOT"] . '/khartn_cms/system/init/footer.php';
?>